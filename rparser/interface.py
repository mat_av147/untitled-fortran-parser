from rparser.subroutine import Subroutine
import rparser.variable

class SubElement():
    def __init__(self):
        self.dtypes = []

class Interface():
    def __init__(self, intname, modref):
        self.intname = intname
        # dict of subroutines
        self.sub_elements = {}
        self.modref = modref

    def update_interface(self, subroutine: Subroutine) -> None:
        if subroutine.subname not in self.sub_elements:
            print("subroutine {} has not been added to interface {}".format(subroutine.subname, self.intname))
        for variable in subroutine.io_vars:
            self.sub_elements[subroutine.subname].dtypes.append(variable.dtype)
            print("interface {} has been updated with subroutine {}".format(self.intname, subroutine.subname))

    def debug(self, n_indent=0):
        output = ""
        return output