from collections import deque

COMP_OPS = ['.EQ.', '.NE.', '.LT.', '.LE.', '.GT.', '.GE.',
            '==', '/=', '<', '<=', '>=', '<', '>']

class ExprNode(object):
    def __init__(self, expr: str, op1: 'ExprNode', op2: 'ExprNode'):
        self.expr = expr
        self.operandL = op1
        self.operandR = op2
    
    @classmethod
    def fromExpr(cls, e: str) -> 'ExprNode':
        return cls(expr=e, op1=None, op2=None)

    @classmethod
    def fromUnary(cls, e: str, op: 'ExprNode') -> 'ExprNode':
        return cls(expr=e, op1=op, op2=None)

    def obj_output(self):
        output = ""
        output += self.expr + "<"
        if self.operandL is None:
            output += "_"
        else:
            output += self.operandL.obj_output()
        output += ","
        if self.operandR is None:
            output += "_"
        else:
            output += self.operandR.obj_output()
        output += ">"
        return output

    def console_output(self, idx):
        ind = '|'
        output = ""
        output += self.expr + "\n"
        if self.operandL is None:
            output += ind*(idx+1) + "NIL" + "\n"
        else:
            output += ind*(idx+1) + self.operandL.console_output(idx+1)
        if self.operandR is None:
            output += ind*(idx+1) + "NIL" + "\n"
        else:
            output += ind*(idx+1) + self.operandR.console_output(idx+1)
        return output

    def __str__(self):
        return self.console_output(0)

    def __repr__(self):
        return self.obj_output()

    def find(self, val):
        if self.expr == val: 
            return self
        if self.operandL is not None and self.operandL.find(val) is not None:
            return self.operandL
        if self.operandR is not None and self.operandR.find(val) is not None:
            return self.operandR
        return None

    def find_intent(self, val, i):
        # assume we're looking down a branch
        # want to return 1 if there is a match, and 2 if the match fits an ignore rule
        intent = 1
        left = 1
        right = 1
        if self.expr == val:
            intent = i
        if self.operandL is not None:
            left = self.operandL.find_intent(val, i)
        if self.operandR is not None:
            right = self.operandR.find_intent(val, i)
        if self.expr in COMP_OPS:
            if self.operandL is not None:
                left = self.operandL.find_intent(val, 2)
            if self.operandR is not None:
                right = self.operandR.find_intent(val, 2)
        elif self.expr == "!IDX":
            if self.operandR is not None:
                right = self.operandR.find_intent(val, 2)
        intent *= left * right
        return intent

    def get_intent(self, val):
        # assume we're at a `=`, want to find matches on the LHS and RHS.
        # If a match exists on the LHS, return OUT
        # If a match exists on the RHS, return IN
        # If both, return INOUT
        # comparison operators and array indices need special ignore rules
        if self.operandL is not None:
            left = self.operandL.find_intent(val, 3)
        else:
            left = 1
        if self.operandR is not None:
            right = self.operandR.find_intent(val, 2)
        else:
            right = 1
        intent = left * right
        #print(left, right)
        if intent % 6 == 0:
            return 6
        if intent % 3 == 0:
            return 3
        if intent % 2 == 0:
            return 2
        return 1


class AstFortran(object):
    def __init__(self, wordlist, verbose=0):
        # in order to build the abstract syntax tree, the shunting yard algorithm is used
        #
        # some modifications are needed to make sure this works with unary operators
        # the previous token is stored in the operator stack so that one can discern the type of token
        # preceding each operator. This determines whether it is a unary or a binary operator.
        # The implementation is quite rough and may not work in all cases. Further testing required.
        try:
            from graphviz import Digraph
            self.graphic = "graphviz"
        except ModuleNotFoundError:
            self.graphic = None

        self.op_stack = deque()
        self.ex_stack = deque()
        self.verbose = verbose

        self.inbuiltfunctions = [
            'DBLE',
            'CMPLX',
            'EXP',
            'ABS'
        ]

        self.unary_ops = {
            '-': 9,
            '(': 0,
            ')': 0,
        }
        
        self.binary_ops = {
            ',': -1,
            '!IDX': 15,
            '(': -100,
            ')': 15,
            '**': 10,
            '*': 9,
            '/': 9,
            '+': 8,
            '-': 8,
            '//': 6,
            '.EQ.': 5,
            '.NE.': 5,
            '.LT.': 5,
            '.LE.': 5,
            '.GT.': 5,
            '.GE.': 5,
            '==': 5,
            '/=': 5,
            '<=': 5,
            '>=': 5,
            '<': 5,
            '>': 5,
            '.NOT.': 4,
            '.AND.': 3, 
            '.OR.': 2,
            '.XOR.': 1,
            '.EQV.': 1,
            '.NEQV.': 1,
            '=': 0,
            '.': 16,
            ':': 12
        }

        previous_token = None
        token_stack = []

        for idx, word in enumerate(wordlist):
            self.debug("processing", word)
            if word == '(':
                if previous_token == "EX":
                    # should be accessing an array
                    self.op_stack.append(("!IDX", previous_token))
                    # trial fix
                    self.op_stack.append(('(', previous_token))
                    self.debug("added to op stack: ", self.op_stack)
                    previous_token = "OP"
                    continue
                self.op_stack.append((word, "EX"))
                self.debug("added to op stack: ", self.op_stack)
                previous_token = "OP"

            elif word == ')':
                self.debug("close parens...")
                self.debug("current op_stack", self.op_stack)
                while(True):
                    try:
                        operator, prev = self.op_stack.pop()
                    except IndexError:
                        self.generate_graphic("graphs/error.gv")
                        raise
                    if operator == '(':
                        break
                    e2 = self.ex_stack.pop()
                    if prev == "EX":
                        e1 = self.ex_stack.pop()                
                        self.ex_stack.append(ExprNode(operator, e1, e2))
                        self.debug("appended binary op", operator, self.ex_stack)
                    else:
                        # expect unary
                        self.ex_stack.append(ExprNode.fromUnary(operator, e2))
                        self.debug("appended unary op", operator, self.ex_stack)

                    #if operator == '!IDX':
                    #    break

            elif self.eq_inbuiltfunction(word):
                continue

            elif self.eq_operator(word):
                self.debug("operator...".format(word))
                if word == ":":
                    # check for special case when the slice takes hidden arguments, add dummy leaves?
                    if wordlist[idx-1] == "," or wordlist[idx-1] == "(":
                        self.ex_stack.append(ExprNode.fromExpr("DUMMY"))
                        previous_token = "EX"
                while(True):
                    if len(self.op_stack) == 0:
                        self.debug("0 len")
                        break
                    self.debug("op_stack", self.op_stack)
                    self.debug("ex_stack", self.ex_stack)
                    operator, prev = self.op_stack.pop()
                    self.debug("popped", operator, prev, "from op stack")
                    if self.precedence(operator, word, prev, previous_token):
                        self.debug("precedence condition not met")
                        self.op_stack.append((operator, prev))
                        break
                    e2 = self.ex_stack.pop()
                    self.debug("popped", e2.__repr__(), "prev", previous_token, "from ex stack")
                    if prev == "EX":
                        try:
                            e1 = self.ex_stack.pop()
                        except IndexError:
                            # Something went wrong that I don't fully understand
                            # Assume structure <EX, OP> and next EX hasn't been
                            # encountered yet, so just break
                            self.ex_stack.append(e2)
                            break       
                        self.debug("popped", e1.__repr__()) 
                        self.ex_stack.append(ExprNode(operator, e1, e2))
                        self.debug("appended binary op", operator, self.ex_stack)
                    elif previous_token == "EX":
                        # expect unary
                        self.ex_stack.append(ExprNode.fromUnary(operator, e2))
                        self.debug("appended unary op", operator, self.ex_stack)
                    else:
                        # unary chain
                        self.debug("unary chain")
                        self.op_stack.append((operator, prev))
                        self.ex_stack.append(e2)
                        break
                self.op_stack.append((word, previous_token))
                self.debug("op stack, ", self.op_stack)
                previous_token = "OP"
                if word == ":":
                    # check for special case when the slice takes hidden arguments, add dummy leaves?
                    if wordlist[idx+1] == "," or wordlist[idx+1] == ")":
                        self.ex_stack.append(ExprNode.fromExpr("DUMMY"))
                        previous_token = "EX"
                
            else:
                # variable
                self.ex_stack.append(ExprNode.fromExpr(word))
                self.debug("appended leaf", self.ex_stack)
                previous_token = "EX"

           # print(self)

        # final processing of operator stack 
        self.debug("length of op_stack", len(self.op_stack))
        while len(self.op_stack) > 0:
            self.debug("op_stack", self.op_stack)
            self.debug("ex_stack", self.ex_stack)
            operator, prev = self.op_stack.pop()
            e2 = self.ex_stack.pop()   
            if prev == "EX":
                try:
                    e1 = self.ex_stack.pop() 
                except IndexError:
                    self.ex_stack.append(ExprNode.fromUnary(operator, e2))
                    return
                self.ex_stack.append(ExprNode(operator, e1, e2))
            else:
                self.ex_stack.append(ExprNode.fromUnary(operator, e2))

    def __str__(self):
        return self.ex_stack[-1].__str__()

    def __repr__(self):
        return self.ex_stack[-1].__repr__()

    def eq_operator(self, word):
        if word in self.binary_ops or word in self.unary_ops:
            return True
    
    def eq_inbuiltfunction(self, word):
        if word in self.inbuiltfunctions:
            return True
    
    def precedence(self, op1, op2, prev1, prev2):
        if prev1 == "EX":
            left = self.binary_ops[op1]
        else:
            left = self.unary_ops[op1]
        if prev2 == "EX":
            right = self.binary_ops[op2]
        else:
            right = self.unary_ops[op2]
        self.debug("precedence", op1, prev1, op2, prev2, left, right)
        return left < right
    
    def debug(self, *args):
        if self.verbose > 0:
            print(args)

    def get_intent(self, varname):
        expr_root = self.ex_stack[-1]
        # traverse until `=` is found, expect one or zero
        expr_eq = expr_root.find('=')
        # now get which branch the varname is in
        return(expr_eq.get_intent(varname))

    def generate_graphic(self, filename):
        if not self.graphic:
            print("error: graphics modules cannot be loaded, make sure graphviz is installed")
            return
        
        from graphviz import Digraph
        f = Digraph('abstract_syntax_tree', filename=filename)
        f.attr('node', shape='circle')

        node = self.ex_stack[-1]
        node_stack = deque()
        inorder_stack = []
        while len(node_stack) > 0 or node is not None:
            if node is not None:
                node_stack.append(node)
                node = node.operandL
                continue
            inorder_stack.append(node_stack.pop())
            node = inorder_stack[-1].operandR

        for node in inorder_stack:
            f.node(str(id(node)), label=node.expr)

        for node in inorder_stack:       
            if node.operandL is not None:
                f.edge(str(id(node)), str(id(node.operandL)))
            if node.operandR is not None:
                f.edge(str(id(node)), str(id(node.operandR)))

        f.view()


if __name__ == "__main__":
    # f_aux( mc + ( j - 1 ) * dfft%nnp ) = f_in( j + ( i - 1 ) * nppx )
    #a = AstFortran(['DFFT%NNP', '(', 'DFFT%NNP', '+', '(', 'J', '-', '1', ')', '*', 'DFFT%NNP', ')', '=', 'F_IN', '(', 'J', '+', '(', 'I', '-', '1', ')', '*', 'NPPX', ')'], 1)
    #a = AstFortran(['A', '(', 'B', ')', '=', 'C', '+', 'D'])
    #l = ['AUX', '(', 'GVECT%NL', '(', '1', ':', 'GVECT%NGM', ')', ')', '=', 'CMPLX', '(', 'AUX1', '(', '1', ',', '1', ':', 'GVECT%NGM', ')', ',', 'AUX1', '(', '2', ',', '1', ':', 'GVECT%NGM', ')', ',', 'KIND', '=', 'DP', ')']
    
    #ast = AstFortran(['a', '(', 'b', ':', 'c', ')'])
    #ast = AstFortran(['EWALDG', '=', 'EWALDG', '+', 'FACT', '*', 'ABS', '(', 'RHON', ')', '**', '2', '*', 'EXP', '(', '-', 'GG', '(', 'NG', ')', '*', 'TPIBA2', '/', 'ALPHA', '/', '4.D0', ')', '/', 'GG', '(', 'NG', ')', '/', 'TPIBA2'], verbose=1)
    #ast = AstFortran(['EXP', '(', '-', 'GG', '(', 'NG', ')', '*', 'TPIBA2', '/', 'ALPHA', '/', '4.D0', ')', '/', 'GG', '(', 'NG', ')', '/', 'TPIBA2'], verbose=1)
    #ast = AstFortran(['EWALDG', '=', 'EWALDG', '+', 'FACT', '*', 'ABS', '(', 'RHON', ')', '**', '2', "*", 't'], verbose=1)
    print(ast.__repr__())   
    ast.generate_graphic("graphs/test.gv")
            
