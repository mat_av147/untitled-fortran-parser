import json

class ParserSettings():
    def __init__(self, settings=None):
        """constructor
        
        Arguments:
            json {dict(str->any)} -- expects a json formatted input
        """
        # defaults
        self.line_limit = 120

        if settings is None:
            # load manually
            with open("../settings.json", 'r') as jsonfile:
                settings = json.load(jsonfile)

        if "line_limit" in settings:
            self.line_limit = settings["line_limit"]