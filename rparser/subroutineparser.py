"""Parser for any subroutine focused fortran files (i.e. files without derived data types)
"""

# std python imports
import copy
import sys
import logging

# local imports
from rparser.fortranparser import FortranParser
from rparser.subroutine import Subroutine
from rparser.interface import Interface, SubElement
from rparser.wordlist import WordList, generate_output
from collections import OrderedDict
from rparser.ast_fortran import AstFortran
from rparser.variable import Variable, Intent
from rparser.shape import get_shape, add_shape, is_deferred_shape

class SubroutineParser(FortranParser):
    """Parser stores all useful information from a linked file, for referencing and printing

    Inherits attributes and methods from FortranParser
    
    Attributes:
        root {Subroutine}: root subroutine, all subroutines in the module are child elements of root
        internal_calls {OrderedDict(str->Subroutine)}: Contains a mapping of pointers to subroutines that show up in call statements
        external_calls {dict(str->Subroutine)}: Contains a mapping of pointers to all external libraries linked to the module
        interfaces {dict(str->Interface)}: Contains a mapping of pointers to interfaces that show up in modules
        current_subroutine {Subroutine}: Pointer to for the subroutine currently being worked on
        current_call {str}: Key for the call currently being worked on
        current_interface {str}: Key for the interface currently being worked on
        temp_output {str}: Stores a running list of content in the case of wrapped lines
        TODO consider moving some of the functions elsewhere, this class is quite big
    """

    def __init__(self, settings):
        super().__init__(settings)
        # some of these indices will need to be lists to handle
        # the cases with more than one subroutine in a file

        self.root = Subroutine("root", self, "ROOT")
        # map of call name -> subroutine object
        self.external_calls = {}
        # map of interface name -> interface object
        self.interfaces = {}
        # try and make this a reference to the current subroutine
        self.current_subroutine = self.root
        self.current_call = ""
        self.current_interface = ""
        self.temp_output = ""
        self.aliases = []

    def __str__(self):
        output_str = super().__str__()
        indent = " " * self.debug_indent
        output_str += "ROOT:\n"
        for key in self.root.content:
            output_str += "{}{}> {}\n".format(indent, key, self.root.content[key])
        output_str += "SUBROUTINES:\n"
        for sub in self.root.children:
            output_str += "{}{}: {{\n".format(indent, sub)
            output_str += self.root.children[sub].debug(n_indent=2)
            output_str += "{}}}\n".format(indent)
        output_str += "INTERFACES:\n"
        for ice in self.interfaces:
            output_str += "{}{}\n".format(indent, ice)
            output_str += self.interfaces[ice].debug(n_indent=2)
            output_str += "{}}}\n".format(indent)
        return output_str

    def init_subroutines(self):
        """ Quickly goes through the file and stores the names of all the subroutines for cross-referencing

        The function is used as an initial pass through before the derived datatype replacement
        """
        correction = 0
        if self.module_added:
            correction = 1
        if not self.file_handle:
            # TODO implement error handling
            #print("no file has been attached to the parser!")
            return
        
        # read in the file 
        self.current_subroutine = self.root
        with open("input/"+self.file_handle+".f90", 'r') as reader:
            for idx, line in enumerate(reader):
                wordlist = WordList(line)
                wordlist.clean()
                # maybe use the enums here
                if not wordlist.cleanlist:
                    continue
                if wordlist.cleanlist[0] == "SUBROUTINE" or (
                        "FUNCTION" in wordlist.cleanlist and not wordlist.cleanlist[0] == "END"):
                    dtype = ""
                    if wordlist.cleanlist[0] == "SUBROUTINE":
                        stype = "SUBROUTINE"
                        subname = wordlist.cleanlist[1]
                    else:
                        stype = "FUNCTION"
                        fidx = wordlist.cleanlist.index("FUNCTION")
                        subname = wordlist.cleanlist[fidx+1]
                        if fidx > 0:
                            dtype = wordlist.cleanlist[fidx-1]
                    if subname in self.current_subroutine.children:
                        sub_ref = self.current_subroutine.children[subname]
                        if sub_ref.subidx >= 0 and sub_ref.subidx != idx:
                            oldname = subname
                            count = self.count_subroutines_by_name(oldname)
                            subname = subname + "!{}".format(count)
                            self.current_subroutine.children[subname] = Subroutine(
                                oldname, self, stype, dtype, self.current_subroutine)
                    else:
                        self.current_subroutine.children[subname] = Subroutine(
                            subname, self, stype, dtype, self.current_subroutine)
                    self.current_subroutine.children[subname].subidx = idx + correction
                    self.current_subroutine = self.current_subroutine.children[subname]
                elif wordlist.cleanlist[0] == "END" and (
                        wordlist.cleanlist[1] == "SUBROUTINE" or wordlist.cleanlist[1] == "FUNCTION"):
                    self.current_subroutine = self.current_subroutine.parent

    def update_dependencies(self):
        """ Dependencies from one subroutine file to another. Fixes the list in the case of complete imports.

        TODO local USE statements are not handled. 
        """
        for dep in self.dependencies:
            # dep is a key that refers to a list of dependencies
            if not self.dependencies[dep]:
                # list is empty, implying that all the subroutines within the module need to be linked
                try:
                    depsubroutines = self.parsers[dep].root.children
                    for key in depsubroutines:
                        if depsubroutines[key].interface is None:
                            # only add the subroutine if it's not part of an interface
                            self.dependencies[dep].append(key)
                except KeyError:
                    # probably a dependency that doesn't need to be linked right now
                    # TODO handle this better
                    continue
                except AttributeError:
                    # probably looking into the wrong parser, just ignore
                    continue

                # now try to add all the interfaces
                try:
                    depinterfaces = self.parsers[dep].interfaces
                    for key in depinterfaces:
                        self.dependencies[dep].append(key)
                except KeyError:
                    continue
                except AttributeError:
                    continue
                #print(self.dependencies)

    def link_externals(self, external_calls):
        """called externally to link the dictionary of external calls so that the module can access it
        
        Arguments:
            external_calls {dict(Subroutine)} -- specified by the input file
        """
        self.external_calls = external_calls

    def update_intent(self, subroutine, varname, intent, verbose=0):
        for line in subroutine.content:
            if varname.upper() in subroutine.content[line]:
                temp = 0
                varidx = subroutine.content[line].index(varname.upper())
                if verbose > 0:
                    print(line, subroutine.content[line])
                # is on the LHS or the RHS
                if "WRITE" in subroutine.content[line]:
                    # write statement, assume that nothing else is going on in the line, `intent` is just in
                    temp = 2 # IN
                elif "DO" in subroutine.content[line]:
                    # do statement, assume that the intent is in
                    temp = 2 # IN
                # CASE 1: CALL statements
                elif "CALL" in subroutine.content[line] and subroutine.content[line].index("CALL") < varidx:
                    # assume line is of format [CALL, CALLNAME, ...]
                    callname = subroutine.content[line][1]
                    # figure out the declaration index of the var
                    decl_index = 0
                    n_param = 0
                    for element in subroutine.content[line]:
                        if element == '(':
                            n_param += 1
                        elif element == ')':
                            n_param -= 1
                        elif element == ',' and n_param == 1:
                            decl_index += 1
                        if element == varname.upper() and n_param == 1:
                            break
                    if callname in subroutine.calls:
                        callsubroutine = subroutine.calls[callname]
                        #print(decl_index, callsubroutine.subname, varname, subroutine.content[line])
                        try:
                            callvarintent = callsubroutine.io_vars[decl_index].intent
                        except IndexError:
                            breakpoint()
                        #print("callvar=", callvarintent)
                        if callvarintent == Intent.OUT:
                            temp = 3
                        elif callvarintent == Intent.IN:
                            temp = 2
                        elif callvarintent == Intent.INOUT:
                            temp = 6
                    elif callname in self.external_calls and verbose > 0:
                        print(self.module, callname)
                # CASE 2: `=` statement, get the index of `=` and check if the varname
                elif "=" in subroutine.content[line]:
                    ast_ = AstFortran(subroutine.content[line])
                    temp = ast_.get_intent(varname.upper()) 
                # CASE 3: comparisons, should be always an `IN` intent
                elif "IF" in subroutine.content[line]:
                    temp = 2
                elif "ALLOCATE" in subroutine.content[line]:
                    temp = 2
                if verbose > 0:
                    print("temp: {}, intent: {}".format(temp, intent))
                # END CASE, now update intent ONLY if the value has not already been overwritten
                if temp != 0 and intent % 3 != 0:
                    intent *= temp

        return intent


    def determine_intent(self, subroutine, variable, verbose=0):
        """returns the intent of a variable
        
        Arguments:
            subroutine {Subroutine} -- the subroutine that determines scope
            variable {Variable} -- the variable we are checking
            verbose {int} -- 0 no verbosity, higher = more verbosity
        """
        intent = variable.intent
        varname = variable.varname
        if verbose > 0:
            print("Determining intent of {}: {}".format(subroutine.subname, varname))
        
        intent = self.update_intent(subroutine, varname, intent, verbose)

        if subroutine.children:
            for child in subroutine.children:
                intent = self.update_intent(subroutine.children[child], varname, intent, verbose)       

        for var in subroutine.vars:
            for dim in subroutine.vars[var].shape:
                if varname in str(dim):
                    intent *= 2

        if verbose > 0:
            print("INTENT: {}".format(intent))

        if intent % 6 == 0:
            variable.intent = Intent.INOUT
        elif intent % 3 == 0:
            variable.intent = Intent.OUT
        elif intent % 2 == 0:
            variable.intent = Intent.IN
        else:
            variable.intent = Intent.NONE

    def inspect_types(self, wordlist, subroutine):
        """determines any derived data types from a WordList object
        
        Arguments:
            wordlist {WordList} -- contains all information about a certain line
            subroutine {Subroutine} -- the subroutine that new variables are added to 
        """

        for i, word in enumerate(wordlist.cleanlist):
            if word in self.objects:
                continue

            # if word starts with %, then except something of format a(b)%c, need to fix this but
            # first handle the object
            if word.startswith('%'):
                n_parens = 0
                head = None
                for fword in reversed(wordlist.cleanlist[:i]):
                    if fword == ",":
                        continue
                    elif fword == ")":
                        n_parens += 1
                    elif fword == "(":
                        n_parens -= 1
                    elif n_parens == 0:
                        head = fword
                        break

                word = head + word

                print("temp get word", word)

            head = word.split("%")[0]
            try:
                tail = word.split("%")[1]
            except IndexError:
                tail = None
            if head in self.objects:
                datatype = self.objects[head]["TYPE"]
                modulename = None
                for key in self.dependencies:
                    # iterate through here and match the VALUE of self.dependencies
                    # with the datatype
                    if datatype in self.dependencies[key]:
                        modulename = key
                
                if not modulename:
                    #print("Module for {} has not been set up in dependencies".format(datatype))
                    return

                if modulename not in self.parsers:
                    #print("Module {} has not been linked correctly".format(modulename))
                    #print("Parser list: {}".format(self.parsers))
                    return

                parser = self.parsers[modulename]
                if tail not in parser.members:
                    #print("Tail = {}".format(tail))
                    #print("No match found in the datatype, parser contains {}".format(parser.members))
                    continue

                word = word.upper()
                
                # add to new_variables if not in already

                new_vars = []

                subroutine = subroutine.backtrack()

                if word not in subroutine.vars:
                    v = subroutine.vars[word] = parser.members[tail].clone()
                    v.varname = head.upper() + "%" + tail.upper()
                    v.parent = head
                    v.replace = head.lower() + "_" + tail.lower()
                    v.derived = True
                    v.update_array_declaration()

                    if self.objects[head]["ARRAY"]:
                        # need to modify the shape in the event of an array of derived datatypes
                        shape_mod = self.objects[head]["SHAPE"]
                        subroutine.prepend_shape(word, shape_mod)
                    new_vars = subroutine.fix_shape(word)
                    subroutine.add_to_io(subroutine.vars[word])
                    #if wordlist.raw == r"call simpson (rgrid(nt)%msh, aux, rgrid(nt)%rab, rhocgnt (1) )":
                    #   breakpoint()

                if subroutine.vars[word].array:
                    shape = parser.members[tail].shape
                    add_shape(wordlist, i, shape, self.current_subroutine.vars)

                for new_var in new_vars:
                    if new_var in subroutine.vars:
                        continue
                    tail = new_var.split('%')[1]
                    v = subroutine.vars[new_var] = parser.members[tail].clone()
                    v.varname = head.upper() + "%" + tail.upper()
                    v.parent = head
                    v.replace = head.lower() + "_" + tail.lower()
                    v.derived = True
                    subroutine.add_to_io(subroutine.vars[new_var])
        
        wordlist.fix_derived_arrays()

    def variable_mode(self, wordlist, line_number, derived=False, verbose=1):
        """adds variables to the current subroutine
        
        Arguments:
            wordlist {WordList} -- line being parsed
            line_number {int} -- normally used for debugging

        TODO perhaps move this to subroutine, since it does not use any other components
        """
        # normally variable declaration has the pattern TYPE, MODIFIER :: name(shape), ...
        #self.inspect_types(wordlist, self.current_subroutine)

        comma_indices = []
        parens = 0
        for idx, val in enumerate(wordlist.cleanlist):
            if val == "(":
                parens += 1
            if val == ")":
                parens -= 1
            if val == "," and parens == 0:
                comma_indices.append(idx)
        try:
            dcolon_index = wordlist.cleanlist.index("::")
        except ValueError:
            # :: is OPTIONAL in variable declaration if no modifiers are present
            # if it doesn't exist then just set it to 0 (this may break?)
            dcolon_index = 0
        comma_indices.append(dcolon_index)
        dtype = wordlist.cleanlist[0]
        if wordlist.cleanlist[1] == '(':
            kind = ""
            idx = 2
            while wordlist.cleanlist[idx] != ')':
                kind += wordlist.cleanlist[idx]
                idx += 1
        else:
            kind = None
        modifiers = []
        intent = Intent.NONE
        for i in comma_indices:
            if i < dcolon_index:
                if wordlist.cleanlist[i+1] == "INTENT":
                    try:
                        intent = Intent[wordlist.cleanlist[i+3]]
                    except KeyError:
                        breakpoint()
                else:
                    modifiers.append(wordlist.cleanlist[i+1])
            else:
                try:
                    varname = wordlist.cleanlist[i+1]
                except IndexError:
                    print(self.module, line_number, wordlist)
                    raise
                try:
                    if wordlist.cleanlist[i+2] == '(':
                        array = True
                    else:
                        array = False
                except IndexError:
                    array = False
                shape = []
                shape.append([])
                if array:
                    shape, _, _ = get_shape(wordlist, i + 2, self.current_subroutine.vars)
                    # maybe need to consider when shape is a variable or a formula...

                    # if the shape consists of derived datatypes need to store these
                    self.inspect_types(wordlist, self.current_subroutine)

                if verbose > 0:
                    print("processing variable: {}, {}".format(varname, shape))

                if derived:
                    # derived datatype, don't add to the variable list, but add to objects instead
                    datatype = wordlist.cleanlist[2]
                    # TODO make this a class?
                    self.objects[varname] = {}
                    self.objects[varname]["TYPE"] = datatype
                    if array:
                        self.objects[varname]["ARRAY"] = True
                        self.objects[varname]["SHAPE"] = shape
                    else:
                        self.objects[varname]["ARRAY"] = False
                    try:
                        self.current_subroutine.remove_var_by_name(varname)
                    except KeyError:
                        print("ERROR: type cannot be found???")
                        print(self.module, line_number, wordlist)
                        breakpoint()
                    continue

                if varname not in self.current_subroutine.vars:
                    self.current_subroutine.vars[varname] = Variable(varname, self.current_subroutine, False)
                self.current_subroutine.vars[varname].modifiers = modifiers
                self.current_subroutine.vars[varname].intent = intent
                self.current_subroutine.vars[varname].dtype = dtype
                self.current_subroutine.vars[varname].kind = kind
                self.current_subroutine.vars[varname].array = array
                self.current_subroutine.vars[varname].shape = shape

                if "PARAMETER" in modifiers:
                    # need to store initial value
                    if wordlist.cleanlist[i+2] != "=":
                        print("warning: parameter declaration has unexpected format", self.module, varname)
                        continue
                    idx = i+3
                    init_value = ""
                    while True:
                        try:
                            if wordlist.cleanlist[idx] == ",":
                                break
                            init_value += wordlist.cleanlist[idx]
                        except IndexError:
                            break
                        idx += 1

                    self.current_subroutine.vars[varname].initial_value = init_value

    def subroutine_mode(self, wordlist, line_number, stype="SUBROUTINE"):
        """If mode == subroutine
        Assume that init_subroutines has been called
        
        Arguments:
            wordlist {Wordlist} -- Contains information about the line being parsed
            line_number {int} -- used for debugging
            stype {str} -- subroutine or function
        """
        #print(wordlist, self.current_subroutine)
        nameidx = wordlist.cleanlist.index(stype)
        subname = wordlist.cleanlist[nameidx+1]
        ignore = [',', '(', ')', '&']
        key, self.current_subroutine = self.get_subroutine_by_idx(line_number)

        if "!" in key:
            # sort of hacky, if we are introducing a duplicate subroutine, force it to have the 
            # same variable list as the original
            self.root.children[key].copy_variables(self.root.children[subname])

        if self.current_subroutine.parsed == 1:
            print("skipping {} ...".format(subname))
            return

        for i, word in enumerate(wordlist.cleanlist):
            if i < 2 + nameidx or word in ignore:
                continue
            if word not in self.current_subroutine.vars:
                #print("adding {} to {}".format(word, self.current_subroutine.subname))
                #print(self.current_subroutine.vars)
                newvar = Variable(word, self, False)
                # both containers may contain derived datatypes, these will be handled 
                self.current_subroutine.vars[word] = newvar
                self.current_subroutine.io_vars.append(newvar)
                self.current_subroutine.variable_order.append(word)
        
        self.current_subroutine.declaration = wordlist.cleanlist

        # link the interface, if the subroutine is part of an interface
        for ikey in self.interfaces:
            if subname in self.interfaces[ikey].sub_elements:
                self.current_subroutine.interface = self.interfaces[ikey]
                self.interfaces[ikey].update_interface(self.current_subroutine)

    def interface_mode(self, wordlist, line_number, step):
        """If mode == interface
        
        Arguments:
            wordlist {WordList} -- Contains information about the line being parsed
            line_number {int} -- used for debugging
            step {int} -- 0 declares interface, 1 declares subroutines
        """
        if step == 0:
            intname = wordlist.cleanlist[1]
            self.current_interface = intname
            self.interfaces[intname] = Interface(intname, self)
        elif step == 1:
            for idx, word in enumerate(wordlist.cleanlist):
                if idx < 2:
                    # skip module procedure keywords
                    continue
                elif word == ',':
                    continue
                else:
                    self.interfaces[self.current_interface].sub_elements[word] = SubElement()

    def preprocess_mode(self, line, line_number):
        write_preprocess = True
        for npp, _ in self.preprocess:
            if line_number == npp:
                write_preprocess = False
        if write_preprocess:
            self.preprocess.append((line_number, line.rstrip("\n")))
            if line.split()[0].upper() == "#DEFINE":
                alias = line.split()[1]
                self.aliases.append(alias)

    def count_subroutines_by_name(self, subname):
        """count the number of subroutines with a particular name

        Note that this name is not the same as the key
        
        Arguments:
            subname {str} -- the name to find in this module
        
        Returns:
            int -- the number of matches
        """
        count = 0
        for key in self.root.children:
            keyname = key.split("!")[0]
            if keyname == subname:
                count += 1
        return count

    def get_subroutine_by_idx(self, idx):
        """find a child subroutine by index (searches current subroutine)
        
        Arguments:
            idx {int} -- the line number
        
        Returns:
            Subroutine -- the subroutine found (currently does not handle failure)
        
        TODO: merge with above somehow?
        """
        for key in self.current_subroutine.children:
            if self.current_subroutine.children[key].subidx == idx:
                return key, self.current_subroutine.children[key]

    def link_call(self, line_number):
        """LINK CALL TO MAP `self.internal_calls` SO THAT WE CAN ACCESS THE SUBROUTINE EASILY
        
        Returns:
            int -- status:
                0 = success
                1-3 = failure (ignored)
                4 = redo
        """
        callmodule = None

        # (1) External call
        if self.current_call.upper() in self.external_calls:
            # currently this option does nothing at all!
            # TODO, perhaps handle the external call so that we know what it does
            return 0
        # (2) Already called subroutine
        elif self.current_call in self.current_subroutine.calls:
            return 0
        # (3) Internal subroutine
        elif self.current_call in self.root.children:
            self.current_subroutine.calls[self.current_call] = self.root.children[self.current_call]
            return 0
        # (4) Check direct children
        elif self.current_subroutine and self.current_call in self.current_subroutine.children:
            self.current_subroutine.calls[self.current_call] = self.current_subroutine.children[self.current_call]
            return 0
        # (5) subroutine in another parser
        else:
            for key in self.dependencies:
                if self.current_call in self.dependencies[key]:
                    callmodule = key

        
        # (6) get this parser
        if callmodule is None:
            # external call, load in linked info
            try:
                self.current_subroutine.calls[self.current_call] = self.external_calls[self.current_call.lower()]
                print("external call loaded", self.current_call)
                return 0
            except KeyError:
                print("external call not found", self.current_call)
                print(self.current_subroutine.calls)
                return 1
        else:
            try:
                if callmodule not in self.parsers:
                    print("{} module not added to setup file".format(callmodule))
                    return 1
                if self.current_call in self.parsers[callmodule].root.children:
                    self.current_subroutine.calls[self.current_call] = self.parsers[callmodule].root.children[self.current_call]
                    return 0
                elif self.current_call in self.parser[callmodule].interfaces:
                    print("Not implemented yet")
                    sys.exit()
                else:
                    print("{}, {}: {} module not set up or subroutine {} not found in linked parser".format(
                        line_number, self.module, callmodule, self.current_call))
                    return 1
            except AttributeError:
                # trying to read the wrong parser type?
                return 2

    def is_internal_call(self, call):
        if call in self.root.children or call in self.current_subroutine.children:
            return True
        return False

    def call_mode(self, wordlist, line_number, verbose=2):
        """If mode == call
        
        Arguments:
            wordlist {Wordlist} -- Contains information about the line being parsed
            line_number -- for debugging
            verbose -- 0: no debug, higher = more debug output

        This is a paricularly long function, consider breaking it down
        """
        # inspect any types first
        self.inspect_types(wordlist, self.current_subroutine)

        # initialize vars
        replacement_vars = []

        try:
            self.current_call = wordlist.cleanlist[1]
        except IndexError:
            print("unexpected call statement", wordlist.cleanlist)
            return 1

        if verbose > 0:
            print("call_mode", line_number, self.module, self.current_subroutine.subname, self.current_call)
            print(wordlist.cleanlist)

        # (1) Adds the call to internal calls
        status = self.link_call(line_number)
        # if the call cannot be found then return immediately
        if status > 0:
            return status

        try:
            callmodule = self.current_subroutine.calls[self.current_call].modref.module
        except AttributeError:
            callmodule = None

        # if call is internal, and subroutine has not been parsed yet, return a flag that tells the parser that
        # the current subroutine needs to be parsed again
        if self.is_internal_call(self.current_call):
            if self.current_subroutine.calls[self.current_call].parsed != 1:
                print("needs a future run")
                self.current_subroutine.parsed = 2 
                return 1
        
        ## (2) Deconstruct the call statement into a list of variables
        statement_vars = wordlist.get_variable_list("CALL")

        # (3) go through the calllist. If there is a datatype, replace it accordingly
        called_io_vars = self.current_subroutine.calls[self.current_call].io_vars
        called_variable_order = self.current_subroutine.calls[self.current_call].variable_order
        vo_idx = 0
        if verbose > 1:
            print(callmodule, self.current_call.upper())
            print('called io vars', called_io_vars)
            print(called_variable_order)
            print("before", statement_vars)
        v_list = self.current_subroutine.calls[self.current_call].vars

        subroutine = self.current_subroutine
        subroutine = subroutine.backtrack()

        for variable in called_io_vars:
            word = variable.varname
            #print(variable)
            if '%' in word:
                # Derived data type, check with the var_order
                head = word.split('%')[0]
                tail = word.split('%')[1]
                while called_variable_order[vo_idx] != head:
                    vo_idx += 1
                # skip strings
                if statement_vars[vo_idx][0] == "!":
                    replacement_vars.append(statement_vars[vo_idx][1:])
                    continue
                new_head = statement_vars[vo_idx]
                new_word = new_head + '%' + tail
                replacement_vars.append(new_word)
                # shape fixing
                new_shape = None
                if variable.shape:
                    #print('shape needs replacement', variable.varname, variable.shape)
                    new_shape = copy.deepcopy(variable.shape)
                    for i, dim in enumerate(new_shape):
                        for j, word in enumerate(dim):
                            if "_" in word and len(word.split('_')) > 1:
                                # try breaking down the word and checking the 'head'
                                s_head = word.split('_')[0]
                                s_tail = word.split('_', 1)[1]
                                if head == s_head.upper():
                                    new_shape[i][j] = new_head + '_' + s_tail
                if new_word not in subroutine.vars:
                    v = subroutine.vars[new_word] = variable.clone()
                    v.varname = new_word
                    v.parent = new_head
                    v.replace = new_head.lower() + "_" + tail.lower()
                    v.derived = True
                    if new_shape is not None:
                        v.shape = new_shape
                    v.update_array_declaration()

                    subroutine.add_to_io(subroutine.vars[new_word])
            else:
                # non derived data type
                try:
                    while called_variable_order[vo_idx] != word and vo_idx < len(called_variable_order):
                        vo_idx += 1
                    # modify strings
                    if not statement_vars[vo_idx]:
                        pass
                    elif statement_vars[vo_idx][0] == "!":
                        replacement_vars.append(statement_vars[vo_idx][1:])
                    else:
                        replacement_vars.append(statement_vars[vo_idx])
                except IndexError:
                    # the number of stored variables don't match up with the number of variables in the call statement.
                    # check for OPTIONAL arguments
                    if "OPTIONAL" in v_list[called_variable_order[vo_idx]].modifiers:
                        pass
                    else:
                        raise
        
        replacement_map = {}
        for i, s in enumerate(statement_vars):
            if s in replacement_vars:
                replacement_map[i] = replacement_vars.index(s)

        # additional pass to set the shape of certain arrays (may merge this into the previous loop? I separated it for clarity)
        for idx, variable in enumerate(called_io_vars):
            # if the shape is given then force this onto the new variable, but only if the shape is an IO argument
            try:
                key = replacement_vars[idx]
            except IndexError:
                break
            # if not derived, skip, no way to handle with this code
            if not variable.derived:
                continue
            # to be safe for now, only switch this on as a last resort, if the shape is empty
            try:
                if variable.array and subroutine.vars[key].shape:
                    continue
            except KeyError:
                print(subroutine.subname, key)
                breakpoint()
            replace_shape = False
            new_shape = copy.deepcopy(variable.shape)
            shape_list = []
            if variable.array:
                shape_list = [o.varname.upper() for o in called_io_vars]
                # if the variables declared in shape are a subset of the input/output variables
                if variable.get_shape_vars() < set(shape_list):
                    replace_shape = True
            if replace_shape:
                for i, dim in enumerate(variable.shape):
                    for j, element in enumerate(dim):
                        if element.upper() in shape_list:
                            sidx = shape_list.index(element.upper())
                            new_shape[i][j] = replacement_vars[sidx]
                subroutine.vars[key].shape = new_shape

        if verbose > 1: 
            print("after", replacement_vars)

        def is_head(a, b):
            if len(b.split("%")) != 2:
                return False
            if b.split("%")[0] == a:
                return True
            return False

        # (4) finally compare the two variable lists and insert new variables
        vo_idx = -1
        ign_comma = False
        final_call_list = []
        for idx, word in enumerate(wordlist.cleanlist):
            if word in statement_vars and word not in replacement_vars:
                if vo_idx == -1:
                    vo_idx += 1
                while vo_idx < len(replacement_vars) and not is_head(word, replacement_vars[vo_idx]):
                    vo_idx += 1
                while vo_idx < len(replacement_vars) and is_head(word, replacement_vars[vo_idx]):
                    final_call_list.append(replacement_vars[vo_idx])
                    final_call_list.append(',')
                    vo_idx += 1
                ign_comma = True
            elif ign_comma and word == ",":
                ign_comma = False                    
            else:
                final_call_list.append(word)
                # check whether this breaks anything
                if word in replacement_vars:
                    vo_idx = replacement_vars.index(word) + 1

        wordlist.cleanlist = final_call_list

        # fix the possible addition of a comma after the last declared derived datatype (panics if the array is too small?!)
        if wordlist.cleanlist[-2] == ',':
            del wordlist.cleanlist[-2]

        return 0

    def check_allocate_call(self, wordlist):
        """checks a line with ALLOCATE to see if it refers to a derived variable

        If derived, we want to remove the allocation and set the shape in the input

        TODO this function shouldn't be used currently since any allocations of derived data types
        are expected to be removed before parsing. Should make sure this doesn't mess up anything.
        
        Arguments:
            wordlist {[WordList]} -- Contains information about the line being parsed
        
        Returns:
            bool -- removes this line from the final output
        """
        skip = False
        self.inspect_types(wordlist, self.current_subroutine)

        varname = wordlist.cleanlist[2]
        if varname in self.current_subroutine.vars:
            if self.current_subroutine.vars[varname].derived:
                # remove allocate call
                skip = True
                # set the shape of the array based of the allocation made
                shape = wordlist.get_shape(3)
                self.current_subroutine.vars[wordlist.cleanlist[2]].shape = shape

        return skip

    def subroutine_generate_output(self, subroutine, i):
        """generates the output for a single subroutine
        
        Arguments:
            subroutine {Subroutine} -- the subroutine to be printed
            i {int} -- indentation
        
        Returns:
            str -- the output string
        """
        indent = ' ' * self.output_indent
        output = ""

        while self.preprocess and subroutine.subidx > self.preprocess[0][0]:
            preprocess_tuple = self.preprocess.popleft()
            output += "{}\n".format(preprocess_tuple[1])

        # write the subroutine declaration
        output += "{}{}{} {} ( ".format(i * indent, subroutine.dtype, subroutine.stype, subroutine.subname.lower())
        line_length = (i * len(indent) + len(subroutine.dtype) + len(subroutine.stype) + 
            len(subroutine.subname))
        if line_length > self.settings.line_limit:
            output += "&\n{}&".format((i+1) * indent)
            line_length = 0
        i += 1

        for idx, var in enumerate(subroutine.io_vars):
            newvar = var.replace
            if idx == 0:
                line_length += len(newvar)
                if line_length > self.settings.line_limit:
                    output += "&\n{}&".format((i+1) * indent)
                    line_length = len(newvar)
                output += "{}".format(newvar)
            else:
                line_length += len(newvar) + 2
                if line_length > self.settings.line_limit:
                    output += "&\n{}&".format((i+1) * indent)
                    line_length = len(newvar) + 2
                output += ", {}".format(newvar)
        output += " )\n"

        # if in the off-chance that `varidx` is not set, and `vars` is, then just throw the declarations here
        if subroutine.varidx == -1 and subroutine.vars:
            output += subroutine.generate_declarations(i)

        # subroutine body
        for key in subroutine.content:

            while self.preprocess and int(key) > self.preprocess[0][0]:
                preprocess_tuple = self.preprocess.popleft()
                output += "{}\n".format(preprocess_tuple[1])

            line = subroutine.content[key]
            if line and (line[0] == "ELSE" or line[0] in self.alwaysout):
                i -= 1
            # regular output
            output += "{}{}\n".format(i * indent, generate_output(
                line, i, indent, self.settings.line_limit, subroutine, self.aliases))

            if line and (line[0] == "ELSE" or line[0] in self.alwaysin):
                i += 1
            elif line and line[0] == "IF" and line[-1] == "THEN":
                i += 1
            # weird case where `DO` is not at the start of a line
            elif "DO" in line and not "END" in line:
                i += 1
            if key == subroutine.varidx:
                output += subroutine.generate_declarations(i)

        try:
            final_line_number = key
        except UnboundLocalError:
            # subroutine content is empty?
            self.write_to_file()
            print(self.module, subroutine)
            raise

        # recurse over children
        for child in subroutine.children:
            temp, final_line_number = self.subroutine_generate_output(subroutine.children[child], i)
            output += temp

        #print(self.preprocess, key)
        # TODO hacky
        while self.preprocess and int(key+2) > self.preprocess[0][0]:
            preprocess_tuple = self.preprocess.popleft()
            output += "{}\n".format(preprocess_tuple[1])

        # end subroutine
        i -= 1
        output += "{}END {} {}\n".format(i * indent, subroutine.stype, subroutine.subname.lower())

        return output, final_line_number

                    

    def module_generate_output(self):
        """generates the output for the whole module handled by this parser
        
        Returns:
            str -- the output string
        """
        i = 0 # the indentation
        indent = ' ' * self.output_indent
        output = ""

        # may want to explicitly declare the module instead of storing it in `outer`

        # write the `outer` things
        # TODO some of the outer things are after subroutine declarations, typically are preprocessor directives
        # need to define a `deque` to store anything that is a preprocessor directive to add them back in unchanged
        for key in self.root.content:
            while self.preprocess and int(key) > self.preprocess[0][0]:
                preprocess_tuple = self.preprocess.popleft()
                output += "{}\n".format(preprocess_tuple[1])

            if self.root.content[key] and self.root.content[key][0] in self.alwaysout:
                i -= 1
            output += "{}{}\n".format(i * indent, generate_output(self.root.content[key], i, indent))
            if self.root.content[key] and self.root.content[key][0] in self.alwaysin:
                i += 1
            # statement MODULE PROCEDURE should not have indentation
            if (len(self.root.content[key]) > 1 
                and self.root.content[key][0] == "MODULE" 
                and self.root.content[key][1] == "PROCEDURE"):
                
                i -= 1

        
        # write each subroutine
        for sub_name in self.root.children:
            output += self.subroutine_generate_output(self.root.children[sub_name], i)[0]

        # end module
        i -= 1
        output += "{}END MODULE {}\n".format(i * indent, self.module.lower())

        return output

                

    def replace_derived_types(self):
        #print("RUNNING REPLACE DERIVED TYPES FOR {}".format(self.file_handle))
        """goes through the file and writes a replacement file without
        any derived datatypes
        """
        correction = 0
        if self.module_added:
            correction += 1
            module_declaration = WordList("MODULE {}".format(self.module))
            module_declaration.clean()
            self.root.content[0] = module_declaration.cleanlist
            self.root.content[1] = ["CONTAINS"]
        if not self.file_handle:
            # TODO implement error handling
            #print("no file has been attached to the parser!")
            return 1
        
        # read in the file
        with open("input/"+self.file_handle+".f90", 'r') as reader:
            # counter to keep track of any line continuations
            line_continuation = 0
            # mode of parsing
            mode = "NONE"
            # current subroutine
            self.current_subroutine = self.root

            # read line by line
            for idx, line in enumerate(reader):
                line_number = idx - line_continuation + correction
                wordlist = WordList(line)
                # if line continuation, continue
                if wordlist.check_wrap():
                    line_continuation += 1
                    self.temp_output += wordlist.raw
                    continue
                elif self.temp_output:
                    wordlist = WordList(self.temp_output + wordlist.raw)
                    self.temp_output = ""
                    line_continuation = 0
                    mode = "NONE"
                else:
                    line_continuation = 0
                    mode = "NONE"

                wordlist.clean(self.aliases)
                skip = False
                # maybe use the enums here
                if not wordlist.cleanlist:
                    continue
                if mode == "NONE":
                    mode = wordlist.cleanlist[0]
                # skip until end of subroutine, in the event that the parser is going through for a second time
                if self.current_subroutine.stype != "ROOT" and self.current_subroutine.parsed == 1:
                    if mode != "END" or wordlist.cleanlist[1] != "SUBROUTINE":
                        continue
                if mode == "!":
                    # comment! just skip
                    continue
                elif mode[0] == "#":
                    # preprocess directive, store unchanged
                    self.preprocess_mode(line, line_number)
                    skip = True
                elif mode == "SUBROUTINE":
                    self.subroutine_mode(wordlist, line_number)
                    skip = True
                elif mode == "FUNCTION" or ("FUNCTION" in wordlist.cleanlist and mode != "END"):
                    # unlike subroutines, function may be preceded by the primitve type it returns
                    self.subroutine_mode(wordlist, line_number, stype="FUNCTION")
                    skip = True
                elif mode == "INTERFACE":
                    self.interface_mode(wordlist, line_number, 0)
                elif mode == "MODULE" and wordlist.cleanlist[1] == "PROCEDURE":
                    self.interface_mode(wordlist, line_number, 1)
                elif mode == "USE":
                    # TODO add subroutine local dependency logic
                    if self.current_subroutine.stype == "ROOT":
                        pass
                    else:
                        self.current_subroutine.varidx = line_number
                    if wordlist.cleanlist[1] in self.parsers and self.parsers[wordlist.cleanlist[1]].__class__.__name__ == "DatatypeParser":
                        skip = True
                elif mode == "IMPLICIT":
                    # this is the point we need to remember for adding in all the new
                    # variables later
                    if self.current_subroutine.stype == "ROOT":
                        pass
                    else:
                        # what if there is no IMPLICIT NONE?
                        #print(self.module, idx)
                        self.current_subroutine.varidx = line_number
                elif mode == "TYPE":
                    skip = True
                    # TYPE CASE, want to break this
                    # 1: get the file that this type is in and store the name
                    self.variable_mode(wordlist, line_number, derived=True)
                elif mode == "ALLOCATE":
                    skip = self.check_allocate_call(wordlist)
                elif mode == "CALL":                
                    self.call_mode(wordlist, line_number)
                elif mode == "END" and wordlist.cleanlist[1] == "SUBROUTINE":
                    self.current_subroutine.complete_parse()
                    self.current_subroutine = self.current_subroutine.parent
                    skip = True
                elif mode == "END" and wordlist.cleanlist[1] == "FUNCTION":
                    self.current_subroutine = self.current_subroutine.parent
                    skip = True
                elif mode == "END" and wordlist.cleanlist[1] == "MODULE":
                    skip = True
                elif mode == "INTEGER" or mode == "LOGICAL" or mode == "REAL" or mode == "COMPLEX" or mode == "CHARACTER":
                    # perhaps define some primitive set based on these types
                    # here we have a primitive variable declaration, store the variable and trash the output (it will be rewritten)
                    if self.current_subroutine.stype != "ROOT":
                        self.variable_mode(wordlist, line_number)
                        skip = True
                else:
                    # 2: inspect for any stored types
                    self.inspect_types(wordlist, self.current_subroutine)

                # 3: store line using idx (line number) as the key
                #print(wordlist.generate_output())
                # TODO test remove if/else, this shouldn't be necessary anymore
                if not skip:
                    if self.current_subroutine.stype == "ROOT":
                        self.root.content[line_number] = wordlist.cleanlist
                    else:
                        self.current_subroutine.content[line_number] = wordlist.cleanlist

        # determine intent (only necessary if the datatype is derived)...
        for sub_name in self.root.children:
            subroutine = self.root.children[sub_name]
            for key in subroutine.vars:
                variable = subroutine.vars[key]
                if variable.derived:
                    self.determine_intent(subroutine, variable)
                # or if the intent is unknown but is passed in/out
                elif (variable.intent == Intent.NONE and
                    variable in subroutine.io_vars):
                    self.determine_intent(subroutine, variable)
                # if intent IN, check shape for validity, if it's invalid, then fix
                # if intent OUT, shape cannot be undefined, raise warning if this is the case
                variable.finalize_shape()
                    


        # replace all of the derived datatypes
        # (TEMP) fix kind DP->8
        self.root.replace_derived_types()

        # decide whether a redo is required
        redo = False
        for sub_name in self.root.children:
            print("{}: {}".format(sub_name, self.root.children[sub_name].parsed))
            if self.root.children[sub_name].parsed == 2:
                redo = True

        if redo:
            return 4
                
        with open("output/"+self.file_handle+".f90", 'w') as writer:
            output = self.module_generate_output()
            writer.write(output)

    
        return 0
