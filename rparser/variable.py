import copy
import regex as re
from enum import IntEnum
from typing import List

from rparser.wordlist import WordList
from rparser.shape import get_shape, shape_to_str, is_deferred_shape

class Intent(IntEnum):
    NONE = 1
    IN = 2
    OUT = 3
    INOUT = 6

class DummyVariable():
    def __init__(self):
        # Consider having an object for the variable name / shape / initial value combo
        self.varnames = [] # can define multiple variables
        self.shapes = [] # list of all shape objects
        self.ivals = [] # list of all initial values
        self.container = None
        self.derived = False
        self.dtype = "NONE"
        self.kind = None
        self.alloc = False
        self.setter = False # has `=` been encountered?
        self.declarer = False # has `::` been encountered?
        
    def to_variables(self):
        if self.varnames is None:
            return None
        varlist = []
        for idx, var in enumerate(self.varnames):
            v = Variable(var, self.container, self.derived)
            v.dtype = self.dtype
            v.kind = self.kind
            v.shape = self.shapes[idx].copy()
            if v.shape:
                v.array = True
            if self.alloc:
                v.modifiers.append("ALLOCATABLE")
            if self.ivals[idx] is not None:
                v.initial_value = self.ivals[idx]
            varlist.append(v)
        return varlist

    def parse_declaration_line(self, wordlist: WordList, members: dict, verbose: int) -> List:
        """Populates the object from a FORTRAN line, expects variable declaration

        This function should be able to handle multiple variable declarations in one line,
        and store their sizes as lists, to be distributed into Variable objects later on.

        Assumes a structure like: [KEYWORD(CONTENTS)] :: [VAR(SHAPE)=INITIAL], where
        square braces denote a comma-separated list

        Args:
            wordlist (WordList): string to parse
            members (dict): variable dictionary to expand
            verbose (int): verbosity for extra debugging output

        Returns:
            List: additional members to add to the variable dictionary
        """
        additional_members = []
        declaration = False
        global_shape = []
        i = 0
        iprev = 0
        try:
            self.dtype = wordlist.cleanlist[0]
            if wordlist.cleanlist[1] == '(':
                i = 2
                self.kind = ""
                while wordlist.cleanlist[i] != ')':
                    self.kind += wordlist.cleanlist[i]
                    i += 1
                if self.kind == "DP":
                    self.kind = '8'
            i += 1
            while True:
                if wordlist.cleanlist[i] == '::':
                    declaration = True
                elif wordlist.cleanlist[i] == ',':
                    pass
                elif declaration:
                    self.varnames.append(wordlist.cleanlist[i])
                    self.ivals.append(None)
                    shape, additional_members, i = get_shape(wordlist, i, members)
                    if global_shape:
                        self.shapes.append(global_shape)
                    else:
                        self.shapes.append(shape)
                    if wordlist.cleanlist[i+1] == '=':
                        i += 2
                        while wordlist.cleanlist[i] != ',':
                            if self.ivals[-1] == None:
                                self.ivals[-1] = wordlist.cleanlist[i]
                            else:
                                self.ivals[-1] += wordlist.cleanlist[i]
                            i += 1
                else:
                    if wordlist.cleanlist[i] == "ALLOCATABLE":
                        self.alloc = True
                    elif wordlist.cleanlist[i] == "POINTER":
                        self.alloc = True
                    elif wordlist.cleanlist[i] == "DIMENSION":
                        global_shape, additional_members, i = get_shape(wordlist, i, members)
                i += 1
        except IndexError:
            return additional_members
    
    # def parse_declaration_line(self, wordlist: WordList, members: dict, verbose: int) -> List:
    #     """Populates the object from a FORTRAN line, expects variable declaration

    #     This function should be able to handle multiple variable declarations in one line,
    #     and store their sizes as lists, to be distributed into Variable objects later on
    #     """
    #     idx = -1
    #     parens_counter = 0
    #     additional_members = []
    #     shape = []
    #     varname = ""
    #     ival = ""
    #     tokendict = {
    #         "INTEGER": lambda: setattr(self, "dtype", "INTEGER"),
    #         "REAL": lambda: setattr(self, "dtype", "REAL"),
    #         "LOGICAL": lambda: setattr(self, "dtype", "LOGICAL"),
    #         "COMPLEX": lambda: setattr(self, "dtype", "COMPLEX"),
    #         "CHARACTER": lambda: setattr(self, "dtype", "CHARACTER"),
    #         "=": lambda: setattr(self, "declarer", True),
    #         "::": lambda: setattr(self, "setter", True),
    #         "ALLOCATABLE": lambda: setattr(self, "alloc", True),
    #         "POINTER": lambda: setattr(self, "alloc", True),
    #     }
    #     while idx + 1 < len(wordlist.cleanlist):
    #         #import pdb; pdb.set_trace()
    #         idx += 1
    #         token = wordlist.cleanlist[idx]
    #         if self.declarer:
    #             # everything after an `=` token should be added here as a string, 
    #             # we'll probably never use this again so no need to parse further
    #             if token == '(':
    #                 parens_counter += 1
    #             elif token == ')':
    #                 parens_counter -= 1
    #             elif token == ',' and parens_counter == 0:
    #                 self.declarer = False
    #                 continue
    #             ival += token
    #         elif token == "DIMENSION":
    #             shape, additional_members, idx = get_shape(wordlist, idx, members)
    #         elif token == ',':
    #             # move on to the next variable if it has been set
    #             if varname:
    #                 self.shapes.append(shape)
    #                 self.varnames.append(varname)
    #                 self.ivals.append(ival)
    #                 # reset these variables
    #                 shape = []
    #                 varname = ""
    #                 ival = ""
    #         elif token == '(':
    #             # set the kind
    #             self.kind = wordlist.cleanlist[idx+1]
    #             if self.kind == "DP":
    #                 self.kind = 8
    #             idx += 2
    #         elif token in tokendict:
    #             tokendict.get(token)()
    #         elif self.setter:
    #             # a variable name has been declared, store this and retrieve the shape of the
    #             # array, if the variable is one
    #             varname = token
    #             if not shape:
    #                 shape, additional_members, idx = get_shape(wordlist, idx, members)

    #     # finally, add all remaining temporary objects to their respective lists
    #     self.shapes.append(shape)
    #     self.varnames.append(varname)
    #     self.ivals.append(ival)

    #     return additional_members
            

class Variable():
    def __init__(self, varname, container, derived):
        self.debug_indent = 4
        self.varname = varname
        self.container = container
        self.derived = derived
        self.intent = Intent.NONE
        self.parent = None
        self.replace = varname.lower()
        self.dtype = "NONE"
        self.modifiers = []
        self.kind = None
        self.array = False
        self.shape = []
        self.initial_value = None

    def __str__(self):
        output_str = ""
        output_str += "{}".format(self.dtype)
        if self.kind:
            output_str += "({})".format(self.kind)
        for modifier in self.modifiers:
            output_str += ", {}".format(modifier)
        output_str += " :: "
        output_str += "{}".format(self.replace)
        if self.array:
            output_str += "{}".format(shape_to_str(self.container.members, self.shape))
        return output_str

    def __repr__(self):
        return self.varname

    def debug(self, n_indent):   
        indent = " " * self.debug_indent
        prepend = indent * n_indent
        output_str = ""
        output_str += "{}VARNAME: {}\n".format(prepend, self.varname)
        output_str += "{}DERIVED: {}\n".format(prepend, self.derived)
        output_str += "{}INTENT: {}\n".format(prepend, self.intent.name)
        output_str += "{}PARENT: {}\n".format(prepend, self.parent)
        output_str += "{}REPLACE: {}\n".format(prepend, self.replace)
        output_str += "{}DTYPE: {}\n".format(prepend, self.dtype)
        output_str += "{}MODIFIERS: {}\n".format(prepend, self.modifiers)
        output_str += "{}KIND: {}\n".format(prepend, self.kind)
        output_str += "{}ARRAY: {}\n".format(prepend, self.array)
        output_str += "{}SHAPE: {}\n".format(prepend, self.shape)
        output_str += "{}INITIAL VALUE: {}\n".format(prepend, self.initial_value)
        return output_str

    def clone(self):
        cloned = Variable(self.varname, self.container, self.derived)
        cloned.intent = self.intent
        cloned.parent = self.parent
        cloned.replace = self.replace
        cloned.modifiers = copy.deepcopy(self.modifiers)
        cloned.dtype = self.dtype
        cloned.kind = self.kind
        cloned.array = self.array
        cloned.shape = copy.deepcopy(self.shape)
        return cloned

    def get_shape_vars(self):
        shape_vars = set()
        numeric = re.compile(r'^[0-9]+(?i)(e\-?[0-9]+)?$')
        for dim in self.shape:
            for e in dim:
                if e and not numeric.search(e):
                    shape_vars.add(e.upper())
        return shape_vars

    def get_shape_dims(self):
        return len(self.shape)

    def shape_has_mixed_types(self):
        typelist = []
        for dim in self.shape:
            undefined = False
            if not dim:
                return False
            if dim[0] == ":" or dim[-1] == ":":
                undefined = True
            typelist.append(undefined)
        if any(typelist) and not all(typelist):
            return True

    def finalize_shape(self):
        if self.intent == Intent.IN:
            if self.shape_has_mixed_types():
                # fix shape to have undefined dimensions
                shape = []
                d = self.get_shape_dims()
                for _ in range(d):
                    shape.append([':'])
                self.shape = shape

    def update_array_declaration(self):
        # workaround for `ALLOCATABLE or POINTER attribute dictates a deferred-shape-array` compiler error
        if "ALLOCATABLE" in self.modifiers and not is_deferred_shape(self.shape):
            self.modifiers.pop(self.modifiers.index("ALLOCATABLE"))


if __name__ == "__main__":
    w = WordList("REAL(DP), ALLOCATABLE :: gg(:)")                    
    w.clean()
    d = DummyVariable()
    d.parse_declaration_line(w)
    print(d.varnames)