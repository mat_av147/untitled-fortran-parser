import regex
from copy import deepcopy
from typing import Tuple, List

from rparser.wordlist import WordList

def is_shape_empty(shape):
    if not shape:
        return True
    if len(shape) == 1 and not(shape[0]):
        return True
    return False

def get_shape(wordlist: WordList, idx: int, members: List) -> Tuple[List, List, int]:
    """interprets a line of code with array declarations and returns the shape 

    Assumes that original string has a form like:
    "word :: p(a, b, (c+d))"
    where parentheses exist and match on both sides. Assumes an integer index value that
    specifies where in the line to look (in the case of lines where multiple arrays are defined)

    The function will stop as soon as it finds a complete shape definition

    Arguments:
        idx {int} -- position to start getting shape
    """
    try: 
        if wordlist.cleanlist[idx+1] == ',':
            # there is no array declared here
            return [], [], idx
    except IndexError:
        # this is definitely nothing here!
        return [], [], idx

    parens_counter = 0
    shape = []
    additional_members = []
    shape.append([])
    i = idx
    for i, token in enumerate(wordlist.cleanlist):
        if i < idx:
            continue
        if token == '=':
            break
        if token == '(':
            parens_counter += 1
        elif token == ')':
            parens_counter -= 1
            if parens_counter == 0:
                # add shape to dict iff key is not none
                return shape, additional_members, i
        elif parens_counter == 1:
            if token == ',':
                shape.append([])
                continue
            if "%" in token:
                head = token.split("%")[0]
                tail = token.split("%")[1]
            else:
                head = None
                tail = token
            # check if numeric
            if regex.compile(r'^[0-9]+(?i)(e\-?[0-9]+)?$').search(tail):
                pass
            # of symbolic (need to extend for all cases)
            elif regex.compile(r'\-|\:').search(tail):
                pass
            elif tail not in members:
                # shape depends on external datatype
                additional_members.append(tail)
            shape[-1].append(token)
    
    if not shape[0]:
        # TODO print some warning here? (this shouldn't be reached..)
        return [], [], idx
    return shape, additional_members, i

def add_shape(wordlist: WordList, idx: int, shape: List, members: List) -> None:
    current_shape, _, _ = get_shape(wordlist, idx, members)
    ndims = 0
    if is_shape_empty(current_shape):
        ndims = len(shape)
    if ndims == 0:
        return
    wordlist.cleanlist.insert(idx+1, ')')
    for n in range(ndims):
        wordlist.cleanlist.insert(idx+1, ':')
        if n + 1 < ndims:
            wordlist.cleanlist.insert(idx+1, ',')
    wordlist.cleanlist.insert(idx+1, '(')

def update_shape(wordlist: WordList, members: dict, verbose: int) -> List:
    """Updates the member dictionary with array shapes defined by allocate calls

    Returns a list of new members to be defined locally (in order for all array sizes
    to be predetermined)
    """
    additional_members = []
    parens_counter = 0
    arr_name = None
    shape = []
    shape.append([])
    for token in wordlist.cleanlist:
        # Whenever a parenthesis is encountered, increase or decrease some counter
        if verbose > 0:
            print(token, arr_name, parens_counter, shape)
        if token == "(":
            parens_counter += 1
        elif token == ")":
            parens_counter -= 1
        elif parens_counter == 1:
            # this is the name of the dynamic array
            if arr_name is not None:
                members[arr_name].shape = shape  
                shape = []
                shape.append([])
                arr_name = None
            if token == ',':
                continue
            if "%" in token:
                head = token.split("%")[0]
                tail = token.split("%")[1]
            else:
                head = None
                tail = token
            if not head:
                #print("External: {}".format(token))
                break
            # TODO maybe implement a smarter check than just `this`
            elif tail not in members and head != "THIS":
                if verbose > 0:
                    print("Implementation Error: {}".format(token))
                break           
            else:
                arr_name = tail
        elif parens_counter == 2:
            if token == ',':
                shape.append([])
                continue
            if "%" in token:
                head = token.split("%")[0]
                tail = token.split("%")[1]
            else:
                head = None
                tail = token
            # check if numeric
            if regex.compile(r'^[0-9]+(?i)(e\-?[0-9]+)?$').search(tail):
                pass
            # of symbolic (need to extend for all cases)
            elif regex.compile(r'\-|\:').search(tail):
                pass
            elif tail not in members:
                # shape depends on external datatype
                additional_members.append(tail)
            shape[-1].append(tail)
    if arr_name:
        members[arr_name].shape = shape
    return additional_members

def dummy_shape(varname: str, shape: List) -> List:
    additional_members = []
    new_shape = deepcopy(shape)
    for i, dim in enumerate(shape):
        if len(dim) == 1 and dim[0] == ':':
            new_shape[i][0] = "{}_d{}".format(varname, i)
            additional_members.append(new_shape[i][0].upper())
    return new_shape, additional_members

def shape_to_str(members: dict, shape: List) -> str:
    output_str = "("
    for dim in shape:
        for element in dim:
            if element in members:
                element = members[element].replace
            output_str += "{}".format(element)
        output_str += ","
    output_str = output_str[:-1]
    output_str += ")"
    return output_str

def is_deferred_shape(shape: List) -> bool:
    for dim in shape:
        if len(dim) != 1:
            return False
        if dim[0] != ':':
            return False
    return True

if __name__ == "__main__":
    # temporary testing
    shape = [[":"]]
    print(is_deferred_shape(shape))