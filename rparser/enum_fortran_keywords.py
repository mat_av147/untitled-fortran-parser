from enum import Enum

class FortranKeywords(Enum):
    # make the ordering of this more intelligent
    MODULE = 1
    USE = 2
    PRIVATE = 3
    SAVE = 4
    CONTAINS = 5
    SUBROUTINE = 6
    IMPLICIT = 7
    NONE = 8
    REAL = 9
    TYPE = 10
    INTENT = 11
    IN = 12
    OUT = 13
    INOUT = 14
    INTEGER = 15
    ALLOCATABLE = 16
    ALLOCATE = 17
    DEALLOCATE = 18
    IF = 19
    THEN = 20
    ELSE = 21
    ENDIF = 22
    DO = 23
    ENDDO = 24
    ALLOCATED = 25
    END = 26
    ONLY = 27
    DP = 28 # not really a keyword but since it's always uppercase we'll include here
    CALL = 29
    PUBLIC = 30
    WHILE = 31
    ELSEIF = 32
    RETURN = 33
    COMPLEX = 34
    EXTERNAL = 35
    DIMENSION = 36
    INTERFACE = 37
    PROCEDURE = 38
    SELECT = 39
    LOGICAL = 40
    CASE = 41
    DEFAULT = 42
    OPTIONAL = 43