import readline # optional, will allow Up/Down/History in the console
import code

class DebugQuery():
    def __init__(self):
        self.DebugQuery

if __name__ == "__main__":
    variables = globals().copy()
    variables.update(locals())
    shell = code.InteractiveConsole(variables)
    shell.interact()