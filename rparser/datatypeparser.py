from rparser.fortranparser import FortranParser 
from rparser.wordlist import WordList
from rparser.variable import DummyVariable, Variable
from rparser.shape import update_shape, dummy_shape

class DatatypeParser(FortranParser):
    """Parser stores all information about a single datatype, as defined by this module

    Inherits attributes and methods from FortranParser

    Attributes:
        datatype {str}: the name of the derived datatype
        members {dict(str->Variable)}: Contains a mapping of pointers to variable objects
        temp_output {list(str)}: Stores a running list of content in the case of wrapped lines
    """
    def __init__(self, settings=None):
        super().__init__(settings)
        self.datatype = None
        self.members = {}
        self.temp_output = ""

    def __str__(self):
        output_str = super().__str__()
        indent = " " * self.debug_indent
        output_str += "DATATYPE: {}\n".format(self.datatype)
        output_str += "MEMBERS: {\n"
        for var in self.members:
            output_str += "{}{}\n".format(indent, var)
            output_str += self.members[var].debug(2)
        output_str += "}\n"

        return output_str

    def check_variable(self, wordlist: WordList) -> bool:
        """checks whether a variable is a Fortran defined type
        
        Arguments:
            wordlist {WordList} -- the line being parsed
        
        Returns:
            bool -- valid Fortran defined type
        """
        vartype = ["INTEGER", "REAL", "LOGICAL", "COMPLEX", "CHARACTER"] # maybe store elsewhere?
        # 'does wordlist.cleanlist contain any of the strings stored in vartype?'
        return any(item in wordlist.cleanlist for item in vartype)

    def is_array(self, shape):
        for dim in shape:
            if len(dim) > 0:
                return True
        return False

    def store_members(self, no_deferred=False, verbose=0):
        """passes through file and stores any members

        additionally if arrays are defined, search the file for any allocation subroutines
        that reveals the size of the array.
        """
        if not self.file_handle:
            # TODO implement error handling
            print("no file has been attached to the parser!")
            return
        
        # read in the file 
        # TODO set this properly in a settings dict
        with open("/home/mat/codes/quantum-ristretto/rparser/input/"+self.file_handle+".f90", 'r') as reader:
            read_types = 0
            for i, line in enumerate(reader):
                member_name = None
                # now reading line by line
                wordlist = WordList(line)
                # in the case of Fortran wrapped lines, concatenate lines
                if wordlist.check_wrap():
                    self.temp_output += wordlist.raw
                    continue
                elif self.temp_output:
                    wordlist = WordList(self.temp_output + wordlist.raw)
                    self.temp_output = ""
                wordlist.clean()

                # skip blank lines
                if not wordlist.cleanlist:
                    continue
                # TYPE specifies the datatype (note that if TYPE is not found, then the parser shouldn't store anything)
                elif wordlist.cleanlist[0] == "TYPE" and len(wordlist.cleanlist) == 2:
                    self.datatype = wordlist.cleanlist[1]
                    if verbose > 0:
                        print("TYPE:", self.datatype)
                    read_types = 1
                elif read_types == 1 and self.check_variable(wordlist):
                    if verbose > 0:
                        print("reading in {}".format(wordlist.cleanlist))
                    # Define a template variable that will be deep copied once the names are found
                    if wordlist.cleanlist[0] == "CONTAINS" or wordlist.cleanlist[0] == "PROCEDURE":
                        continue
                    tempvar = DummyVariable()
                    additional_members = tempvar.parse_declaration_line(wordlist, self.members, verbose)
                    for m in additional_members:
                        self.add_placeholder(m)
                    tempvar.container = self
                    v_list = tempvar.to_variables()
                    for var in v_list:
                        self.members[var.varname] = var
                        if verbose > 0:
                            print("adding VAR", var)

                elif read_types == 1 and wordlist.cleanlist[0] == "END" and wordlist.cleanlist[1] == "TYPE":
                    read_types = 2
                elif read_types == 2 and "ALLOCATE" in wordlist.cleanlist:
                    # this probably determines the size of a dynamic array
                    wordlist.resolve_operators()
                    if verbose > 0:
                        print("ALLOC", wordlist.cleanlist)
                    additional_members = update_shape(wordlist, self.members, 0)
                    for m in additional_members:
                        self.add_placeholder(m)
                elif read_types == 2 and "NULLIFY" in wordlist.cleanlist:
                    # we probably have a pointer that doesn't have an allocation step.
                    # currently f2py compiler complains that these are undefined, so just add a shape for them

                    # probably not robust, assumes a form `nullify(var)`
                    varname = wordlist.cleanlist[2]
                    if '%' in varname:
                        tail = varname.split('%')[1]
                    else:
                        tail = varname
                    try:
                        v = self.members[tail]
                        new_shape, additional_members = dummy_shape(tail.lower(), v.shape)
                        v.shape = new_shape
                        for m in additional_members:
                            self.add_placeholder(m)
                    except KeyError:
                        # pointer is not a recognized input, probably a derived datatype
                        # currently rystretto handles objects, so when decomposing functions
                        # these variables aren't needed
                        pass

        # finally remove 'allocatable' if not necessary
        for v in self.members.values():
            v.update_array_declaration()
        if no_deferred:
            self.remove_allocatables()

    def add_placeholder(self, label):
        """add a placeholder variable (for now this is only used for the shape)"""
        placeholder = Variable(label, self, False)
        placeholder.dtype = "INTEGER"
        self.members[label] = placeholder

    def remove_allocatables(self):
        additional_members = []
        for key in self.members:
            v = self.members[key]
            if "ALLOCATABLE" in v.modifiers:
                v.shape, addit = dummy_shape(v.varname, v.shape)
                additional_members += addit
        for m in additional_members:
            self.add_placeholder(m)

if __name__ == "__main__":
    """Main testing routine

    Takes in a datatype and prints out all parsed information

    Arguments:
        arg1 {str} -- datatype file to read in
    """
    import sys

    a = DatatypeParser()
    a.link_file(sys.argv[1])
    a.store_members(verbose=0)
    #import pdb; pdb.set_trace()
    for key in a.members:
        print(a.members[key])