import sys
import regex as re

from rparser.enum_fortran_keywords import FortranKeywords

class WordList():
    """handles a single line of code and parses it for later manipulation.

    Additionally converts the line into a format that is easy to reformat. The goal
    is to reformat all code fed in into a consistent format. 
    """
    def __init__(self, input_string, indentation=0):
        self.indentation = indentation
        self.raw = input_string.strip()
        if self.raw and self.raw[0] == '&':
            self.raw = self.raw[1:]
        self.dirtylist = re.findall(r'([\'\"\(\)\,]|[^\(\)\'\"\,\s]+)', self.raw)
        self.cleanlist = []
        self.outputlist = []

    def __str__(self):
        return generate_output(self.cleanlist)

    def check_wrap(self):
        if not self.raw:
            return False
        if '!' in self.raw:
            self.raw = self.raw[:self.raw.index('!')].strip()
        if self.raw and self.raw[-1] == '&':
            self.raw = self.raw[:-1]
            self.dirtylist = re.findall(r'([\'\"\(\)\,]|[^\(\)\'\"\,\s]+)', self.raw)
            return True
        return False
        
    def clean(self, aliases=[]):
        """This function needs to be generalized for all expected Fortran input, currently parsing
        should work in standard cases, but it's not very elegant
        
        Returns:
            [type] -- [description]
        """
        self.cleanlist = []

        # quotation flags, either ON or OFF
        quot = False
        dquot = False

        # write/print flags
        write = 0

        if self.dirtylist and self.dirtylist[0] == "&":
            self.dirtylist = self.dirtylist[1:]

        comment = False
        append_mod = [] 

        for word in self.dirtylist:

            if word[-1] == '&':
                append_mod.insert(0, '&')
                word = word[:-1]

            # if quotations on, then don't do any fancy parsing
            if quot or dquot:
                self.cleanlist.append(word)

            if "'" in word:
                if quot:
                    quot = False
                else:
                    quot = True
                    if not dquot:
                        self.cleanlist.append(word)
                continue

            if '"' in word:
                if dquot:
                    dquot = False
                else:
                    dquot = True
                    if not quot:
                        self.cleanlist.append(word)
                continue

            if quot or dquot:
                continue
                

            # for now just remove comments...
            if "!" in word and not quot and not dquot:
                pre_comment = word.split("!")[0]
                if pre_comment:
                    word = pre_comment
                else:
                    break
                comment = True

            if word and word[-1] == ",":
                append_mod.insert(0, ',')
                word = word[:-1]
            
            if word in aliases:
                print("ALIAS", word, aliases)
                self.cleanlist.append(word)
                continue                
            
            word = word.upper()

            def longoprepl(matchobj):
                if matchobj.group(0) == ".LE.":
                    return ' <= '
                elif matchobj.group(0) == ".GE.":
                    return ' >= '
                elif matchobj.group(0) == ".EQ.":
                    return ' == '
                elif matchobj.group(0) == ".NE.":
                    return ' /= '
                elif matchobj.group(0) == ".LT.":
                    return ' < '
                elif matchobj.group(0) == ".GT.":
                    return ' > '
                elif matchobj.group(0) == ".NOT.":
                    return ' ! '
                elif matchobj.group(0) == ".AND.":
                    return ' !! '
                elif matchobj.group(0) == ".OR.":
                    return ' !@ '
                elif matchobj.group(0) == ".FALSE.":
                    return ' @ '
                elif matchobj.group(0) == ".TRUE.":
                    return ' @@ '
                else:
                    return ''

            def parensrepl(matchobj):
                if matchobj.group(0) == "(":
                    return " ( "
                elif matchobj.group(0) == ")":
                    return " ) "
                else:
                    return ""

            def slicerepl(matchobj):
                if matchobj.group(0) == ":":
                    return " : "

            # try regex
            # (1) replace long comparison operators
            word = re.sub(r'\.\w{2,5}\.', longoprepl, word)
            # (2) replace parentheses
            word = re.sub(r'\(|\)', parensrepl, word)
            # fix slices
            word = re.sub(r'(?<!:):(?!:)', slicerepl, word)
            # (3) split
            valid_token = re.compile(r'([-+]?\d+\.\d*[DE]?[-+]?\d*(?:_DP)?|[a-zA-Z0-9%_#]+|\s+)')
            sliced_word = re.split(valid_token, word)
            for w in sliced_word:
                if w == '' or w == ' ':
                    continue
                elif w == '@':
                    self.cleanlist.append('.FALSE.')
                elif w == '@@':
                    self.cleanlist.append('.TRUE.')
                elif w == '!':
                    self.cleanlist.append('.NOT.')
                elif w == '!!':
                    self.cleanlist.append('.AND.')
                elif w == '!@':
                    self.cleanlist.append('.OR.')
                else:
                    self.cleanlist.append(w)

            if append_mod:
                self.cleanlist.extend(append_mod)
                del append_mod[:]
            
            if comment:
                break


    def fix_derived_arrays(self):
        # any pattern of the kind `a(b)%c` can be safely be converted to `a%c(b)`, the parser will fix anything else
        # any pattern of the kind  a(b)%c(d) can be converted to a%c(b, d) etc.

        # STEP 1 - move the derived object together with the datatype
        for i, word in enumerate(self.cleanlist):
            if word.startswith('%'):
                n_parens = 0
                head = None
                head_idx = 0
                for idx, fword in reversed(list(enumerate(self.cleanlist[:i]))):
                    if fword == ",":
                        continue
                    elif fword == ")":
                        n_parens += 1
                    elif fword == "(":
                        n_parens -= 1
                    elif n_parens == 0:
                        head = fword
                        head_idx = idx
                        break

                if head is not None:
                    self.cleanlist[head_idx] = head + word
                    del(self.cleanlist[i])

                    # STEP 2 - collect array indices
                    if i >= len(self.cleanlist):
                        continue
                    if self.cleanlist[i] == "(":
                        j = i + 2
                        self.cleanlist.insert(i - 1, ',')
                        print(self.cleanlist)
                        while True:
                            print(j, self.cleanlist[j])
                            if j >= len(self.cleanlist):
                                break
                            if self.cleanlist[j] == ")":
                                break
                            self.cleanlist.insert(i, self.cleanlist[j])
                            j += 2
                            i += 1
                    
                        # chop off the duplicated part
                        for _ in range(j - i):
                            del(self.cleanlist[i])

    def resolve_operators(self):
        temp = []
        operation = ""
        for word in self.cleanlist:
            if operation:
                operation += word
                temp[-1] = operation 
                operation = ""
            elif (word == "*"):
                operation = temp[-1] + word
            else:
                temp.append(word)
        self.cleanlist = temp

    def append_varlist(self, varlist):
        del self.cleanlist[-1]
        for v in varlist:
            self.cleanlist.append(v)
        self.cleanlist.append(')')

    def prepend_varlist(self, varlist):
        temp = []
        for idx, token in enumerate(self.cleanlist):
            if idx == 3:
                # little hacky perhaps, EXPECT that this is the beginning position for the CALL variables..
                for var in varlist:
                    temp.append(var)
                    temp.append(',')
            temp.append(token)
        self.cleanlist = temp
        
    def get_variable_list(self, statement):
        """interprets a list of variables from some declaration statement

        Assumes the wordlist has been cleaned
        
        Arguments:
            statement {str} -- CALL
        
        Returns:
            list(str) -- variable names
        """
        statement_vars = []

        if statement == "CALL":
            start_idx = 2
        else:
            start_idx = 0

        parens = 0
        quot = False
        for idx, word in enumerate(self.cleanlist):
            if word == ',':
                if parens == 1:
                    statement_vars.append("")
            elif idx < start_idx:
                pass
            elif word == '(':
                parens += 1
            elif word == ')':
                parens -= 1
            elif word == "'":
                if not quot:
                    quot = True
                else:
                    quot = False
            elif parens > 1:
                pass
            elif statement_vars:
                if quot:
                    statement_vars[-1] = '!' + word
                else:
                    statement_vars[-1] += word
            else:
                statement_vars.append(word)

        return statement_vars


def generate_output(cleanlist, i=0, indent="", linelimit=sys.maxsize, subroutine=None, aliases={}):
    """generates output for a line

    Note that this is not a method of the class since the Wordlist information is not
    kept, to save space (maybe don't do this?). The parsed list is stored in a Subroutine
    object, which is then sent to this function to generate the final output
    
    Arguments:
        cleanlist {list(str)} -- a parsed line in the format of a list
    
    Keyword Arguments:
        i {int} -- indentation (default: {0})
        indent {str} -- indent pattern (default: {""})
        linelimit {int} -- maximum possible size of line (default: {sys.maxsize})
        subroutine {Subroutine} -- pointer to subroutine object that contains the line 
        we are processing in order to replace derived datatype variables. (default: {None})
    
    Returns:
        str -- output string
    """
    output = ""
    skip = 0
    n_chars = i * len(indent)

    # regex matches
    alphanumeric = re.compile(r'^[\w\_\%]+$')
    parens = re.compile(r'^[\(\)]$')
    numeric = re.compile(r'^[0-9]+(?i)(e\-?[0-9]+)?$')
    sci1 = re.compile(r'^[0-9]*(?i)d\-?[0-9]+$')
    sci2 = re.compile(r'^[0-9]+(?i)(e\-?[0-9]+)?_dp$')

    # other matches
    parens_exception = ["IN", "OUT", "INOUT", "DP"]
    
    quot = False
    dquot = False

    for idx, word in enumerate(cleanlist):
        n_chars += len(word)
        if n_chars > linelimit:
            output += "&\n" + (i+2) * indent + "&"
            n_chars = len(word) + (i+2) * len(indent)
        if skip > 0:
            skip -= 1
            continue

        if "'" in word:
            if quot:
                quot = False
            else:
                quot = True

        if '"' in word:
            if dquot:
                dquot = False
            else:
                dquot = True
        
        # a rough way to uppercase fortran keywords
        if word in FortranKeywords.__members__ and not quot and not dquot:
            if idx > 0:
                output += " "
                n_chars += 1
            output += word
        # rough way to fix case of aliases
        elif word in aliases and not quot and not dquot:
            if idx > 0:
                output += " "
                n_chars += 1
            output += word
        # want to fix specific patterns...
        #elif quot or dquot:
        # (1) lists, or `obj1 comma obj2` should only have a space after the comma
        elif word == ",":
            output += ','
        # (2) decimal points need to connect numericals
        elif (word == "." and numeric.search(cleanlist[idx-1]) and (
                sci1.search(cleanlist[idx+1]) or sci2.search(cleanlist[idx+1]) or
                numeric.search(cleanlist[idx+1]))):
            output += '.' + cleanlist[idx+1]
            skip = 1
        # (3) negative variable, `not alphanumeric` + `-` + `alphanumeric`
        elif (not alphanumeric.search(cleanlist[idx-1]) and not (cleanlist[idx-1] == ')')
                and (word == "-") and alphanumeric.search(cleanlist[idx+1])):
            if idx > 0:
                output += " "
                n_chars += 1
            output += "-"+cleanlist[idx+1].lower()
            skip = 1
        # (4) exception to below when we have case statements
        elif word == '(' and cleanlist[idx-1] == "CASE":
            output += " " + word
        # (4) exception to below when we declare intent
        elif word == '(' and cleanlist[idx+1] in parens_exception and cleanlist[idx+2] == ')':
            output += word + cleanlist[idx+1] + cleanlist[idx+2]
            skip = 2
        # (4) open parens shouldn't need a space when succeeding some name
        elif word == '(' and alphanumeric.search(cleanlist[idx-1]):
            output += word
        elif word == '(' and cleanlist[idx+1] == '/':
            output += ' ' + word + cleanlist[idx+1]
            skip = 1
        elif word == ')' and output[-1] == '/':
            output += ')'
        elif (word == ':' and alphanumeric.search(cleanlist[idx-1]) and alphanumeric.search(cleanlist[idx+1]) and
                not cleanlist[idx-1] == "ONLY"):
            output += word + cleanlist[idx+1]
            skip = 1
        # (5) anything in quotations shouldn't need spaces between the quotations themselves
        elif (word == "'" and not quot) or (word == '"' and not dquot):
            output += word
        elif word == "'" or word == '"':
            if idx > 0:
                output += " "
            output += word + cleanlist[idx+1]
            skip = 1
        # (6)
        elif subroutine and word.lower() in subroutine.vars:
            replace = subroutine.vars[word.lower()].replace
            if idx == 0:
                output += replace
            else:
                output += " " + replace

        # otherwise do nothing to the word
        else:
            if idx > 0:
                output += " "
                n_chars += 1
            output += word.lower()

    return output

if __name__ == "__main__":
    # unittests are now separate, maybe call them here
    if len(sys.argv) > 1:
        a = WordList(sys.argv[1])
    else:
        a = WordList("REAL(DP) :: x")
    a.clean()
    print("before regex", a.dirtylist)
    print("after regex", a.cleanlist)
    print(generate_output(a.cleanlist))
    pass