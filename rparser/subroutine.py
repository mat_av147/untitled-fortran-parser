import sys
import logging
import regex
import copy
from collections import OrderedDict
from rparser.variable import Variable, Intent

class Subroutine():
    """Contains all relevant information from a subroutine

    debug_indent {int}: size of the indent used for generating debug files
    output_indent {int}: size of the indent used for generating output files
    declaration {list(str)}: the line of the subroutine declaration
    subname {str}: the name of the subroutine
    stype {str}: the type, subroutine, or function
    dtype {str}: functions have datatypes that determine the type that is outputted by that function
    subidx {int}: the line number of the subroutine declaration
    varidx {int}: the line number where variables are declared
    vars {dict(str->Variable)}: mapping of pointers to variables used by the subroutine
    io_vars {list(Variable)}: input/output variables, note that this should be a subset of vars
    variable_order {list(str)}: the order of the variables passed into the declaration
    content {dict(int->list(str))}: mapping of line numbers to the parsed content of that line
    children {dict(str->Subroutine)}: mapping to any subroutines contained
    parent {Subroutine}: the parent subroutine in the case that this is a child
    interface {Interface}: the interface that this subroutine is a part of, in the event that is the case
    modref {FortranParser}: the module this subroutine exists in
    parsed {int}: Flag that determines if the subroutine has been populated with content
    0 = not parsed
    1 = parsed
    2 = parsed but needs a redo (due to calling future subroutines)
    """
    def __init__(self, subname, modref, stype="SUBROUTINE", dtype="", parent=None):
        self.debug_indent = 4
        self.output_indent = 4
        self.declaration = []
        self.subname = subname
        self.stype = stype
        self.dtype = dtype
        if self.dtype:
            self.dtype = self.dtype + " "
        self.subidx = -1 # in the event that the subroutine key is not unique, this should be truly unique!
        self.varidx = -1
        self.vars = {}
        self.io_vars = []
        self.variable_order = [] # the initial order of input/output variables stored so that io_vars can be constructed consistently
        # ordered dictionary of content
        self.content = OrderedDict()
        # subroutines can be nested! store an ordered dict of the
        # nested subroutines
        self.children = OrderedDict()
        self.parent = parent
        self.interface = None
        self.calls = OrderedDict()
        self.modref = modref
        self.parsed = 0

    def __str__(self):
        return self.subname

    def debug(self, n_indent=0):        
        indent = " " * self.debug_indent
        prepend = indent * n_indent
        output_str = ""
        output_str += "{}DECLARATION: {}\n".format(prepend, self.declaration)
        output_str += "{}STYPE: {}\n".format(prepend, self.stype)
        output_str += "{}DTYPE: {}\n".format(prepend, self.dtype)
        output_str += "{}SUBNAME: {}\n".format(prepend, self.subname)
        output_str += "{}SUBIDX: {}\n".format(prepend, self.subidx)
        output_str += "{}VARIDX: {}\n".format(prepend, self.varidx)
        output_str += "{}VARS: {{\n".format(prepend)
        for var in self.vars:
            output_str += "{}{} {}\n".format(indent * (n_indent+1), id(self.vars[var]), var)
            output_str += self.vars[var].debug(n_indent+2)
        output_str += "{}}}\n".format(prepend)
        output_str += "{}IO_VARS: \n".format(prepend)
        for io in self.io_vars:
            output_str += "{}  {} {}\n".format(prepend, id(io), io.varname)
        output_str += "{}CONTENT: {{\n".format(prepend)
        for line in self.content:
            output_str += "{}{}{}> {}\n".format(prepend, indent, line, self.content[line])
        output_str += "{}}}\n".format(prepend)
        if self.children:
            output_str += "{}CHILDREN: {{\n".format(prepend)
            for sub in self.children:
                output_str += self.children[sub].debug(n_indent=n_indent+1)
            output_str += "{}}}\n".format(prepend)
        return output_str

    def copy_variables(self, subroutine):
        """copies variable dict and io variables from subroutine to self
        
        Arguments:
            subroutine {Subroutine} -- copy origin
        """
        for key in subroutine.vars:
            self.vars[key] = subroutine.vars[key].clone()
        for var in subroutine.io_vars:
            if var.varname in self.vars:
                self.io_vars.append(self.vars[var.varname])
        print("copied:")
        for var in self.io_vars:
            print(var.varname)

    def backtrack(self):
        if self.stype == "ROOT":
            print("ERROR: running backtrack on root!")
            sys.exit()
        elif self.parent.stype == "ROOT":
            return self
        else:
            return self.parent.backtrack()


    def fix_shape(self, varkey):
        """fixes the shape content in the event that the shape is a derived datatype
        
        Arguments:
            varkey {str} -- key to the variable (array)
        
        Returns:
            list(list(str)) -- a list of dimensions. Each dimension is a list that describes the shape for
            that dimension
        """
        #print(varkey, self.vars[varkey].shape)
        # fixes the shape of derived data type arrays
        head = self.vars[varkey].parent.lower()
        shape_vars = []

        for i, dimension in enumerate(self.vars[varkey].shape):
            for j, svar in enumerate(dimension):
                if "%" in svar:
                    # think I can just ignore
                    # TODO make sure this doesn't break things, need to finish unittests!!!
                    continue
                if svar == ":":
                    continue
                test_name = head.upper() + "%" + svar.upper()
                for var in self.io_vars:
                    if test_name == var.varname:
                        # match, go ahead with replacement shape
                        #print("{}, {}: replace {} with {}".format(self.subname, varkey, svar, var.varname))
                        self.vars[varkey].shape[i][j] = var.varname
                        shape_vars.append(test_name)
                        break
                # no match found, is this a variable? if yes, then the shape variable HAS to be declared but isn't. Just
                # 'simulate the creation' for now (hacky) and notify subroutine_parser that we need to create the variable
                if regex.search(r"[a-zA-Z]+", svar):
                    shape_vars.append(test_name)
                    #print("{}, {}: force replace {} with {}".format(self.subname, varkey, svar, head.upper() + "%" + svar.upper()))
                    self.vars[varkey].shape[i][j] = head.upper() + "%" + svar.upper()
                else:
                    # do nothing, it's a number
                    pass
        
        return shape_vars

    def prepend_shape(self, varkey, shape_mod):
        """adds dimensions to the shape (at beginning)
        
        Arguments:
            varkey {str} -- key to the variable
            shape_mod {list(list(str))} -- list of dimensions. Each dimension is a list that describes the shape for that
            dimension
        """
        variable = self.vars[varkey]
        for s in reversed(shape_mod):
            variable.shape.insert(0, s)

        variable.array = True


            
    def remove_var_by_name(self, varname):
        """removes a variable from the input/output variable list
        
        Arguments:
            varname {str} -- name of the variable
        """
        del self.vars[varname]
        for idx, var in enumerate(self.io_vars):
            if var.varname == varname:
                del self.io_vars[idx]

    def _get_var_ordering(self, variable):
        """get the position of a variable in the declaration
        
        Arguments:
            variable {Variable}
        
        Returns:
            int -- position
        """
        if variable.parent is not None:
            #print("ordering", self.variable_order)
            #print("var", variable)
            return self.variable_order.index(variable.parent)
        else:
            return self.variable_order.index(variable.varname)

    def add_to_io(self, variable):
        """adds a variable to the input/output list
        
        Arguments:
            variable {Variable}
        """
        for idx, var in enumerate(self.io_vars):
            orderA = self._get_var_ordering(var)
            orderB = self._get_var_ordering(variable)
            if orderB < orderA:
                self.io_vars.insert(idx, variable)
                return
            elif orderB == orderA:
                # check alphabetical precedence
                if variable.varname < var.varname:
                    self.io_vars.insert(idx, variable)
                    return
        self.io_vars.append(variable)

    def single_declaration(self, variable, i):
        """prepares the output of a single variable declaration
        
        Arguments:
            variable {Variable}
            i {int} -- indentation
        
        Returns:
            str -- output string
        """
        indent = ' ' * self.output_indent
        output = "{}{}".format(i * indent, variable.dtype)
        if variable.kind is not None:
            output += "({})".format(variable.kind)
        if variable.modifiers:
            for modifier in variable.modifiers:
                output += ", {}".format(modifier)
        if variable.intent != Intent.NONE:
            output += ", INTENT({})".format(variable.intent.name)
        output += " :: {}".format(variable.replace)
        if variable.array:
            if variable.shape:
                shape = ""
                for dim in variable.shape:
                    for word in dim:
                        shape += word.lower()
                    shape += ","
                shape = shape[:-1]
                #print("shape", shape)
            elif variable.intent == Intent.IN:
                shape = ":"
            else:
                logging.warning("{}, {} has no shape".format(self.subname, variable.varname))
                shape = "NONE"
            output += "({})".format(shape)
        if "PARAMETER" in variable.modifiers:
            output += " = {}".format(variable.initial_value)
        return output, variable.varname, variable.dtype, variable.intent


    def generate_declarations(self, i):
        """Messy way to sort the variable declarations...
        
        Arguments:
            i {[type]} -- [description]
        
        Returns:
            [type] -- [description]
        """
        output = ""
        temp_list_derived_array = []
        temp_list_derived_primitive = []
        temp_list_original = []
        temp_list_params = []

        for var in self.vars:
            if self.vars[var].derived:
                if self.vars[var].array:
                    temp_list_derived_array.append(self.single_declaration(self.vars[var], i))
                else:
                    temp_list_derived_primitive.append(self.single_declaration(self.vars[var], i))
            else:
                if "PARAMETER" in self.vars[var].modifiers:
                    temp_list_params.append(self.single_declaration(self.vars[var], i))
                else:
                    temp_list_original.append(self.single_declaration(self.vars[var], i))
        temp_list_derived_array.sort(key=lambda tup: tup[1])
        temp_list_derived_array.sort(key=lambda tup: tup[2])
        temp_list_derived_array.sort(key=lambda tup: tup[3])
        for val in temp_list_params:
            output += val[0] + '\n'
        for val in temp_list_derived_primitive:
            output += val[0] + "\n"
        for val in temp_list_original:
            output += val[0] + "\n"
        for val in temp_list_derived_array:
            output += val[0] + "\n"
        return output

    def replace_derived_types(self):
        for key in self.content:
            for i, word in enumerate(self.content[key]):
                if word in self.vars:
                    self.content[key][i] = self.vars[word].replace
                if self.parent is not None:
                    # search up a single tier
                    if word in self.parent.vars:
                        self.content[key][i] = self.parent.vars[word].replace
        for child in self.children:
            self.children[child].replace_derived_types()
        for key in self.vars:
            variable = self.vars[key]
            if not variable.shape:
                continue
            for i, dim in enumerate(variable.shape):
                for j, word in enumerate(dim):
                    if word in self.vars:
                        variable.shape[i][j] = self.vars[word].replace
            if variable.kind is not None and str(variable.kind).upper() == "DP":
                variable.kind = 8


    def complete_parse(self):
        for key in self.calls:
            if self.calls[key].parsed != 1:
                print("future run queued for {} due to {}".format(self.subname, self.calls[key].subname))
                self.parsed = 2
                return
        self.parsed = 1
        return


# where should this function go?
def from_external(data):
    """creates a Subroutine from external data

    Used to import a external library, which are all defined by json files, we store the datatype of input/output variables
    
    Arguments:
        data {dict(str->dict(str->any))} -- contents of a json file. Mapping of keys to external `subroutines` which are stored
        as mappings of string labels to information
    
    Returns:
        Subroutine -- the generated subroutine
    """
    output_dict = {}
    for call in data:
        tempsub = Subroutine(call, None, "EXTERNAL")
        try:
            data[call]["n_param"]
        except KeyError:
            print(call, "does not contain n_param key, check the extlib json file for formatting")
        for varidx in range(data[call]["n_param"]):
            try:
                var = data[call][str(varidx)]
            except KeyError:
                print(call, "has a mismatch between variable keys and n_param. Check the extlib json file for formatting")
            
            if var["container"] == "array":
                alloc = True
            elif var["container"] == "pointer":
                alloc = True
            else:
                alloc = False

            if var["dtype"] == "int":
                dtype = "INTEGER"
            elif var["dtype"] == "void":
                dtype = "GENERIC"
            elif var["dtype"] == "complex":
                dtype = "COMPLEX"
            elif var["dtype"] == "logical":
                dtype = "LOGICAL"
            elif var["dtype"] == "real":
                dtype = "REAL"
            else:
                print(var["dtype"], "has not been handled by subroutine.from_external()")

            if var["intent"] == "in":
                intent = Intent.IN
            elif var["intent"] == "out":
                intent = Intent.OUT
            elif var["intent"] == "inout":
                intent = Intent.INOUT
            else:
                intent = Intent.NONE

            tempsub.vars[var["name"]] = Variable(var["name"], tempsub, False)
            tempsub.vars[var["name"]].allocatable = alloc
            tempsub.vars[var["name"]].dtype = dtype
            tempsub.vars[var["name"]].intent = intent

            tempsub.io_vars.append(tempsub.vars[var["name"]])
            tempsub.variable_order.append(var["name"])
            # IMPORTANT!!!
            tempsub.parsed = 1
        output_dict[call] = tempsub
    return output_dict
