"""The parent parser that all the different types of parsers derive from.

This file contains variables that all parsers are expected to handle at some point in time.

The general idea is that each file is handled by a unique parser that contains specific information about the subroutines, and calls that
the file might make to other files. Parsers are then able to communicate with each other in order to fill in any missing gaps.
"""

from collections import deque
from rparser.wordlist import WordList
from rparser.settings import ParserSettings

class FortranParser():
    """
    Attributes:
        file_handle (str): The file handle. Gives the parser the ability to access the file at any time.
        dependencies (dict(str)): Dependencies allow the parser to know what other parsers it needs.
        objects (dict(str->(dict(str->str))): Objects are all the derived data types the parser might need to deal with.
    """
    def __init__(self, settings=None):
        self.file_handle = None
        self.dependencies = {}
        self.objects = {}
        # Files may be a module
        self.module = None
        self.module_added = False
        # Preprocess container
        self.preprocess = deque()
        # Dict of parsers for comminucation between parsers, this is populated based of the dictionary of dependencies.
        # However, the parser list is often merely a subset of the dependency list.
        self.parsers = {}
        # for debugging
        self.debug_indent = 4
        self.output_indent = 4

        # each parser wants access to these, maybe put them in a separate class
        self.alwaysin = {"DO", "MODULE", "INTERFACE", "SUBROUTINE", "FUNCTION", "SELECT"}
        self.alwaysout = {"END", "ENDIF", "ENDDO"}

        # settings to be fed in via settings.json (now accepts default)
        if settings is None:
            self.settings = ParserSettings()
        else:
            self.settings = settings

    def __str__(self):
        indent = " " * self.debug_indent
        output_str = ""
        # handle
        output_str += "FILE: {}.f90\n".format(self.file_handle)
        output_str += "MODULE: {}\n".format(self.module)
        output_str += "DEPENDENCIES: {\n"
        for dep in self.dependencies:
            output_str += "{}{}: [".format(indent, dep)
            for sub in self.dependencies[dep]:
                output_str += "{}, ".format(sub)
            output_str += "]\n"
        output_str += "}\n"
        output_str += "OBJECTS: {\n"
        for obj in self.objects:
            output_str += "{}{}: {{\n".format(indent, obj)
            if not self.objects[obj]:
                continue
            for key in self.objects[obj]:
                val = self.objects[obj][key]
                output_str += "{0}{0}{1}: {2},\n".format(indent, key, val)
            output_str += "{}}}\n".format(indent)
        output_str += "}\n"
        output_str += "PREPROCESS DIRECTIVES: \n"
        output_str += "{}{}\n".format(indent, self.preprocess)

        return output_str        

    def write_to_file(self):
        if not self.file_handle:
            return
        # TODO set this properly in a settings dict
        with open("/home/mat/codes/quantum-ristretto/rparser/debug/"+self.file_handle+".debug", 'w') as writer:
            writer.write(self.__str__())

    def link_file(self, filepath):
        """links a file to the parser
        
        Arguments:
            filepath {PATH} -- relative/absolute file location
        """
        if filepath.endswith('.f90'):
            filepath = filepath[:-4]
        self.file_handle = filepath.lower()

    def init_dependencies(self):
        """reads through the file and figures out dependencies based off the USE statements

        Also stores the module name here, if applicable
        """
        if not self.file_handle:
            # TODO implement error handling
            print("no file has been attached to the parser!")
            return
        
        # read in the file 
        # TODO set this properly in a settings dict
        with open("/home/mat/codes/quantum-ristretto/rparser/input/"+self.file_handle+".f90", 'r') as reader:
            for line in reader:
                # now reading line by line
                wordlist = WordList(line)
                wordlist.clean()
                # maybe use the enums here
                if not wordlist.cleanlist:
                    continue
                elif wordlist.cleanlist[0] == "MODULE" and not wordlist.cleanlist[1] == "PROCEDURE":
                    self.module = wordlist.cleanlist[1]
                    #print("module set to {}".format(self.module))
                elif wordlist.cleanlist[0] == "USE":
                    # USE CASE, want to add to dependencies
                    # expect format: USE MODULENAME COMMA ONLY COLON DEPENDENCYLIST
                    dependencies = []
                    if "ONLY" in wordlist.cleanlist:
                        try:
                            deplist = wordlist.cleanlist[5:]
                            for dep in deplist:
                                if dep != ',':
                                    dependencies.append(dep)
                        except IndexError:
                            print("unexpected format in USE statement (refer to fortranparser.init_dependencies())")
                    self.dependencies[wordlist.cleanlist[1]] = dependencies

        # if there is no module, and the file is a top level dependency it's probably okay...
        # add a fix for now, TODO consider doing this better, either a smarter way to link files, or
        # applying some strict struture rules to incoming fortran files
        if self.module is None:
            self.module = "{}_".format(self.file_handle)
            self.module_added = True
        
        #print(self.dependencies)

    def link_parser(self, plabel, parser):
        """links a reference of a parser to this parser
        
        Arguments:
            plabel {STRING} -- the name of the parser, usually the file it links to
            parser {FORTRANPARSER} -- the parser object
        """
        self.parsers[plabel] = parser
        #print("{} now contains {}".format(self.module, plabel))